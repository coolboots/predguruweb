package com.coolboots.qureka.predGuru;

import java.time.Duration;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.SocketOptions;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;

@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisConfig {

	@Value("${spring.redis.host}")
	String redisHost;
	@Value("${spring.redis.port}")
	int redisPort;

	@Value("${spring.redis.pool.maxTotal}")
	int maxTotal;
	@Value("${spring.redis.pool.maxidle}")
	int maxIdle;
	@Value("${spring.redis.pool.minidle}")
	int minIdle;

	@Value("${spring.redis.timeout}")
	int timeout;

	@Value("${spring.redis.pool.maxWaitMillis}")
	long maxWaitMillis;

	@Value("${spring.redis.pool.minEvictableIdleTimeMillis}")
	long minEvictableIdleTimeMillis;
	
	
	@Bean(destroyMethod = "shutdown")
	ClientResources clientResources() {
	    return DefaultClientResources.create();
	}

	@Bean
	public RedisStandaloneConfiguration redisStandaloneConfiguration() {
	    return new RedisStandaloneConfiguration(redisHost, redisPort);
	}
	
	@Bean
	public ClientOptions clientOptions(){
	    return ClientOptions.builder()
	    		.disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
	            .autoReconnect(true)
	            .socketOptions(socketOptions())
	            .build();
	}
	
	@Bean
	SocketOptions socketOptions() {
		
		return SocketOptions.builder()
				.connectTimeout(Duration.ofMillis(timeout))
				.tcpNoDelay(true)
				.build();
	}
	
	@Bean
	GenericObjectPoolConfig getConfig() {
		
		GenericObjectPoolConfig config = new GenericObjectPoolConfig();
		
		config.setMaxIdle(maxIdle);
		config.setMaxTotal(maxTotal);
		config.setMinIdle(minIdle);
		config.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
		config.setMaxWaitMillis(maxWaitMillis);
		config.setTestOnBorrow(true);
		return config;
	}
	
	@Bean
	LettucePoolingClientConfiguration lettucePoolConfig(GenericObjectPoolConfig poolConfig,ClientOptions options, ClientResources dcr){
	    return LettucePoolingClientConfiguration.builder()
	            .poolConfig(poolConfig)
	            .clientOptions(options)
	            .clientResources(dcr)
	            .build();
	}

	@Bean
	public RedisConnectionFactory connectionFactory(RedisStandaloneConfiguration redisStandaloneConfiguration,
	                                                LettucePoolingClientConfiguration lettucePoolConfig) {
	    return new LettuceConnectionFactory(redisStandaloneConfiguration, lettucePoolConfig);
	}

	@Bean
//	@ConditionalOnMissingBean(name = "redisTemplate")
	@Primary
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
	    RedisTemplate<String, Object> template = new RedisTemplate<>();
	    template.setConnectionFactory(redisConnectionFactory);
	    
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		template.setHashValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		template.setEnableTransactionSupport(true);
	    return template;
	}

	
}
