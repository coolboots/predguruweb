package com.coolboots.qureka.predGuru.utils;

public class DynamicApiKeyConstant {
	
	
	public static final String REGISTER_ENCRYPT_KEY 			= "register_key";
	public static final String REGISTER_ENCRYPT_VALUE			= "YEK-GER-LAICOS-AKERUQ";
	
	public static final String OTP_ENCRYPT_KEY 					= "otpverify_key";
	public static final String OTP_ENCRYPT_VALUE 				= "YfiRevPtO-IPA--AKERUQ";
	
	public static final String PREDICTION_JOIN_ENCRYPT_KEY		= "prediction_join_key";
	public static final String PREDICTION_JOIN_ENCRYPT_VALUE	= "NOITCIDERP-NIOJ-AKERUQ";


	public static final String UPDATE_PROFILE_ENCRYPT_VALUE		= "EtADpU-iforp-IPA-AKERUQ";
	
	
}
