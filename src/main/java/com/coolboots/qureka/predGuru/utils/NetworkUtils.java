package com.coolboots.qureka.predGuru.utils;

import javax.servlet.http.HttpServletRequest;

public class NetworkUtils {
	
	public static String getSubDomain(HttpServletRequest request) {
		String subdomain = request.getServerName().toString();
		if(subdomain.equalsIgnoreCase("localhost")) {
			subdomain = "play.predguru.com";
		}
		
		return subdomain;
	}
}
