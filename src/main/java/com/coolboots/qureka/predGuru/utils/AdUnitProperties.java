package com.coolboots.qureka.predGuru.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class AdUnitProperties {
	public Map<String, Map<String, String>> hosts = new HashMap<>();
	
	public void init(){
		
		Map<String, String> playMap = new HashMap<>();
		playMap.put("Demo_Question_1", "<ins class='adsbygoogle sq_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8994801385' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		playMap.put("Demo_Question_2", "<ins class='adsbygoogle sq_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8989263651' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		playMap.put("Continue_Page", "<ins class='adsbygoogle dh_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='6177066354' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		playMap.put("Intro_Page", "<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='4752304042' data-ad-format='horizontal' data-full-width-responsive='false'></ins>");
		playMap.put("Sticky_Top","<ins class='adsbygoogle _360_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8499977361'></ins>");
		playMap.put("Start_Screen_Top", "<ins class='adsbygoogle dh_ad_2' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='5985494662'></ins>");		
		playMap.put("Pred_Popup_Proceed", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='5682242332'></ins>");
		playMap.put("Popup_Rank_Breakup", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='9429915652'></ins>");
		playMap.put("Point_System_Page", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='2864507304'></ins>");
		playMap.put("Welcome_Popup", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='1359853940'></ins>");
		playMap.put("Coin_Earnings", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='1503324175' ></ins>");
		playMap.put("Pred_Feed", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='9976710569'></ins>");
		playMap.put("Score_Screen_Feed", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='5410779595'></ins>");
		playMap.put("Recent_Winners_Listing", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='3794639118'></ins>");
		playMap.put("Check_Winner_List", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='7803356258'></ins>");
		
		hosts.put("play.predguru.com", playMap);
		
		Map<String,String> tbMap = new HashMap<>();	
		tbMap.put("Demo_Question_1", "<ins class='adsbygoogle sq_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='3954227692' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		tbMap.put("Demo_Question_2", "<ins class='adsbygoogle sq_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='4776354779' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		tbMap.put("Continue_Page", "<ins class='adsbygoogle dh_ad_1' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='5177192914' data-ad-format='auto' data-full-width-responsive='true'></ins>");
		tbMap.put("Intro_Page", "<ins class='adsbygoogle' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8823410998' data-ad-format='horizontal' data-full-width-responsive='false'></ins>");
		tbMap.put("Sticky_Top","<ins class='adsbygoogle _360_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='9837109768'></ins>");
		tbMap.put("Start_Screen_Top", "<ins class='adsbygoogle dh_ad_2' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8524028096'></ins>");		
		tbMap.put("Pred_Popup_Proceed", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='4501022605'></ins>");
		tbMap.put("Popup_Rank_Breakup", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='5622532586'></ins>");
		tbMap.put("Point_System_Page", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='1958619745'></ins>");
		tbMap.put("Welcome_Popup", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='8332456409'></ins>");
		tbMap.put("Coin_Earnings", "<ins class='adsbygoogle _330_50_ad' style='display:block' data-ad-client='ca-pub-7317129100639756' data-ad-slot='1767048054'></ins>");
		tbMap.put("Pred_Feed", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='7865552540'></ins>");
		tbMap.put("Score_Screen_Feed", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='3864111242'></ins>");
		tbMap.put("Recent_Winners_Listing", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='2631754776'></ins>");
		tbMap.put("Check_Winner_List", "<ins class='adsbygoogle' style='display:block' data-ad-format='fluid' data-ad-layout-key='-fb+5w+4e-db+86' data-ad-client='ca-pub-7317129100639756' data-ad-slot='1127101415'></ins>");
		
		hosts.put("tb.predguru.com", tbMap);
	}
	
	public String getAdUnit(String host,String slot){
		try {
			if(host.equals("0:0:0:0:0:0:0:1")) {
				host = "play.predguru.com";
			}
			
			if(hosts.get(host)==null) return "";
			String code = hosts.get(host).get(slot);
			if(code==null) return "";
			
			return code;
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return "";
	}
	
}
