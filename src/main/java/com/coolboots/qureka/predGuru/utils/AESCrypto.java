package com.coolboots.qureka.predGuru.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESCrypto {

	public static final String ENCRYPT_KEY = "Mary has one cat";
	public static final String QUIZ_KEY = "old macdonald ha";
	private final static String HEX = "0123456789ABCDEF";

	public static final String REGISTER_ENCRYPT_KEY = "register api key";
	public static final String OTP_ENCRYPT_KEY = "otpverify by api";
	public static final String BETA_OTP_ENCRYPT_KEY ="betavrify by api"; 
	public static final String SDK_REGISTER_KEY = "reg sdk key apis";
	public static final String UPDATE_PROFILE_ENCRYPTION_KEY = "update key profi";
	public static final String REDEMPTION_ENCRYPTION_KEY = "nBneiwYEtYOQrHvx";
	public static final String GAME_USER_SCORE_KEY = "gAmESoreWitHContEStIDGAmEiD";
	
	public static final String GAME_JOINING_KEY = "jerxewe faere324";
	
	public static void encryptFile(String key128bit, File inputFile, File outputFile) {
		try {
			FileInputStream inputStream = new FileInputStream(inputFile);
			byte[] inputBytes = new byte[(int) inputFile.length()];
			inputStream.read(inputBytes);
			inputStream.close();
			byte[] result = encrypt(key128bit.getBytes(), inputBytes);
			FileOutputStream outputStream = new FileOutputStream(outputFile);
			outputStream.write(result);
			outputStream.close();
		} catch (Exception e) {
		}
	}

	public static byte[] decryptFile(String key128bit, File inputFile) {
		try {
			FileInputStream inputStream = new FileInputStream(inputFile);
			byte[] inputBytes = new byte[(int) inputFile.length()];
			inputStream.read(inputBytes);
			inputStream.close();
			byte[] result = decrypt(key128bit.getBytes(), inputBytes);
			return result;
		} catch (Exception e) {
			return new byte[0];
		}
	}
	
	public static String encryptPlainText(String string) throws Exception {
		return encryptPlainText(AESCrypto.ENCRYPT_KEY, string);
	}

	
	public static String encryptPlainText(String key, String cleartext) throws Exception {
		
		String key128bit = CheckKeyUtil.checkKey(key);
		
		byte[] result = encrypt(key128bit.getBytes(), cleartext.getBytes());
		String fromHex = toHex(result);
		
		String base64 = new String(Base64.encode(fromHex.getBytes(), Base64.NO_WRAP));
//		String base64 = new String(Base64.getEncoder().encodeToString(fromHex.getBytes()));
		return base64;
	}

	public static String decryptPlainText(String key, String encrypted) throws Exception {
		
		String key128bit = CheckKeyUtil.checkKey(key);
		String base64 = new String(Base64.decode(encrypted, Base64.NO_WRAP));
//		String base64 = new String(Base64.getDecoder().decode(encrypted));
		byte[] rawKey = key128bit.getBytes();
		byte[] enc = toByte(base64);
		byte[] result = decrypt(rawKey, enc);
		return new String(result);
	}

	private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(clear);
		return encrypted;
	}

	private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] decrypted = cipher.doFinal(encrypted);
		return decrypted;
	}

	public static String toHex(byte[] buf) {
		if (buf == null)
			return "";
		StringBuffer result = new StringBuffer(2 * buf.length);
		for (int i = 0; i < buf.length; i++) {
			appendHex(result, buf[i]);
		}
		return result.toString();
	}

	private static void appendHex(StringBuffer sb, byte b) {
		sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
	}

	private static byte[] toByte(String hexString) {
		int len = hexString.length() / 2;
		byte[] result = new byte[len];
		for (int i = 0; i < len; i++)
			result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
		return result;
	}
	
    private static final String HASH_ALGORITHM = "SHA-256";

	
	private static SecretKeySpec generateKey(final String key) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
        byte[] bytes = key.getBytes("UTF-8");
        digest.update(bytes, 0, bytes.length);
        byte[] keys = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keys, "AES");
        
        return secretKeySpec;
    }

	public static String encryptionUtil(String key, byte[] plainText) throws Exception {
		
		byte[] IV = new byte[12];
		
		SecretKeySpec keySpec = generateKey(key);
		
		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
	//	cipher = Cipher.ge
		
	    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, IV);
	    //IvParameterSpec gcmParameterSpec = new IvParameterSpec(IV);
	    
	    cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);
	    byte[] cipherText = cipher.doFinal(plainText);
	    
	    String pp = new String(Base64.encode(cipherText, Base64.NO_WRAP));
		//String pp = java.util.Base64.getEncoder().encodeToString(cipherText);

	    return pp;
	}
	
	
	public static String decryptUtils(String key, String cipherText) throws Exception {
		
		SecretKeySpec keySpec = generateKey(key);

		byte[] IV = new byte[12];
		Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, IV);
        //IvParameterSpec gcmParameterSpec = new IvParameterSpec(IV);
        
        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);
        
        //byte[] decryptedText = cipher.doFinal(java.util.Base64.getDecoder().decode(cipherText));
        byte[] decryptedText = cipher.doFinal(Base64.decode(cipherText, Base64.NO_WRAP));
        
        return new String(decryptedText);
	}


}
