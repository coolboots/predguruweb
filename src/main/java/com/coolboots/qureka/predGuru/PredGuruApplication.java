package com.coolboots.qureka.predGuru;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.coolboots.qureka.predGuru.utils.AdUnitProperties;

@SpringBootApplication
@ComponentScan(basePackages = "com.coolboots.qureka.predGuru")
public class PredGuruApplication extends SpringBootServletInitializer implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(PredGuruApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.setConnectTimeout(10000).build();
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PredGuruApplication.class);
    }
	
//	@Autowired
//	YamlAdUnitProperties yamlAdUnitProperties;
//	
	@Autowired 
	AdUnitProperties adUnitProperties;
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Hello");
		try {
			
			adUnitProperties.init();
			
//			System.out.println(yamlAdUnitProperties.getHosts().get("play.qureka.com").get("Demo_Question_1"));
			}catch (Exception e) {
				e.printStackTrace();
			}
		
	}
}
