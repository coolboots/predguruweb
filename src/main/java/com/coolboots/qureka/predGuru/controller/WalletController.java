package com.coolboots.qureka.predGuru.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.api.AppUserApiService;
import com.coolboots.qureka.predGuru.user.api.WalletApiService;
import com.coolboots.qureka.predGuru.user.model.CoinWalletDetails;
import com.coolboots.qureka.predGuru.user.service.UserService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;
import com.coolboots.qureka.predGuru.wallet.service.WalletService;

@Controller
@RequestMapping("/wallet")
public class WalletController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	RedisTemplateService redisTemService;
	
	@Autowired WalletService walletService;
	@Autowired WalletApiService walletApiService;
	@Autowired AppUserApiService appUserApiService;
	@Autowired
	AdUnitProperties adUnitProperties;

	@GetMapping("/coinearning")
	public String getCoinWalletEarning(HttpServletRequest request, ModelMap modelMap) {
		
		try {
		
			if(CookieConfig.getCookies(request, "userId") == null) {
				return "redirect:/intro";
		 	}
			
			String userId = CookieConfig.getCookies(request, "userId");
			
			
			String userWall = redisTemService.readHashValueFromField("userWallet", userId);
			if(userWall.equals("")) {
				return "redirect:/";
			}
			
			WalletDto walletDto = userService.getWalletAmount(userId, userWall);
			modelMap.addAttribute("coinBalance", walletDto.getCoinBalance());
			
			List<CoinWalletDetails> coinWalletDetails = null;
			Response response = walletService.coinWalletDetails(userId);
			if(response.getStatus() == 200) {
				coinWalletDetails = (List<CoinWalletDetails>) response.getObject();

			}
			
			String host = NetworkUtils.getSubDomain(request);
			String Coin_Earnings = adUnitProperties.getAdUnit(host, "Coin_Earnings");
			modelMap.addAttribute("Coin_Earnings",Coin_Earnings);
			
			modelMap.addAttribute("coinWalletDetailsList", coinWalletDetails);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "wallets/coinearnings";
	}
}
