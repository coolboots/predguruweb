package com.coolboots.qureka.predGuru.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.model.QuestionPlainPojo;
import com.coolboots.qureka.predGuru.model.SampleMatchPojo;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;

@Controller
public class MainController {

	@Autowired
	RedisTemplateService redisTemplateService;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	PredictionService predictionService;

//	@Autowired
//	YamlAdUnitProperties adUnits;

	@Autowired
	AdUnitProperties adUnitProperties;

	@GetMapping("/intro")
	public String root(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {

		SampleMatchPojo sampleMatchPojo = predictionService.getSampleQuestions();
		String login = "no";
		if (CookieConfig.getCookies(request, "userId") != null) {
			login = "yes";
		}

		String host = NetworkUtils.getSubDomain(request);

		System.out.println("----------------------" + host + "-------------------------");
		String Demo_Question_1 = adUnitProperties.getAdUnit(host, "Demo_Question_1");
		String Demo_Question_2 = adUnitProperties.getAdUnit(host, "Demo_Question_2");
		System.out.println(Demo_Question_1);
		System.out.println(Demo_Question_2);

		modelMap.addAttribute("sampleMatch", sampleMatchPojo);

		modelMap.addAttribute("login", login);
		modelMap.addAttribute("Demo_Question_1", Demo_Question_1);
		modelMap.addAttribute("Demo_Question_2", Demo_Question_2);

		request.getSession().removeAttribute("redirectSuccess");
		return "index-intro";
	}

	@GetMapping("/intro/question")
	public String introQuestions(HttpServletRequest request, ModelMap modelMap) {

		List<QuestionPlainPojo> pp = predictionService.getPredictionQuestion(request);
		Collections.shuffle(pp);

		modelMap.addAttribute("questionList", pp);
		return "introquestion";
	}

	@PostMapping("/intro/success")
	public String introSuccess(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {

		String teamA = request.getParameter("teamA");
		String teamB = request.getParameter("teamB");
		String teamAUrl = request.getParameter("teamAUrl");
		String teamBUrl = request.getParameter("teamBUrl");

		request.getSession().setAttribute("redirectSuccess", "redirectSuccess");
		request.getSession().setAttribute("addcoins", "1");
		modelMap.addAttribute("teamA", teamA);
		modelMap.addAttribute("teamB", teamB);
		modelMap.addAttribute("teamAUrl", teamAUrl);
		modelMap.addAttribute("teamBUrl", teamBUrl);

		String host = NetworkUtils.getSubDomain(request);
		String Continue_Page = adUnitProperties.getAdUnit(host, "Continue_Page");
		modelMap.addAttribute("Continue_Page", Continue_Page);

		return "introquestion/introsuccess";
	}

	@GetMapping("/termsconditions")
	public String termsAndCondition(HttpServletRequest request, ModelMap modelMap) {
		return "signup/termsandconditions";
	}

	@GetMapping("/privacypolicy")
	public String privacyPolicy(HttpServletRequest request, ModelMap modelMap) {
		return "signup/privacypolicy";
	}

	@GetMapping("/rules")
	public String rules(HttpServletRequest request, ModelMap modelMap) {
		return "rules/rules";
	}

	@GetMapping("/opt")
	public String showOptForm(HttpServletRequest request, ModelMap modelMap) {
		return "optguidlines/donotselloutput";

	}

	@GetMapping("/optout/success")
	public String showOptOutForm(HttpServletRequest request, ModelMap modelMap) {
		return "optguidlines/donotsell";

	}

	@GetMapping("/contact-us")
	public String contactUsPage(HttpServletRequest request, ModelMap modelMap) {
		return "contactus/contact-us";

	}
}
