package com.coolboots.qureka.predGuru.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.Data;

@Controller
public class ScoreSubmitController {
	
	@Autowired RedisTemplateService redisTemplateService;
	

	@PostMapping("/match/scoreSubmit")
	public @ResponseBody Response  questionPlayAjaxs(@RequestBody Data data, HttpServletRequest request) {
		
		Response response = new Response();
		try {
			
			/*if(request.getSession().getAttribute("userId") == null) {
				response.setMessage("error");
				response.setMessage("Blank user id ");
				return response;	   	 	
			}*/
			request.getSession().setAttribute("scorePBack", "reach");

			if(CookieConfig.getCookies(request, "userId") == null) {
				response.setMessage("error");
				response.setMessage("Blank user id ");
				return response;	   	 	
			}
			String userId = CookieConfig.getCookies(request, "userId");
			Long matchId = Long.valueOf(request.getParameter("matchId"));
			if(data.getUserId() == "") {
				response.setMessage("error");
				response.setMessage("Blank user id ");
				return response;
			}
			
			String userJoin = redisTemplateService.readHashValueFromField("match_score_"+userId, ""+matchId);
			
			if(userJoin.equals("")) {
				response.setMessage("unjoin");
				return response;
			}
			if(userJoin.equals("sucess")) {
				response.setMessage("joined");
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	}
	
}
