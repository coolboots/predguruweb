package com.coolboots.qureka.predGuru.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.Data;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.QuestionPlainPojo;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.user.service.UserService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class QuestionController {

	@Autowired
	PredictionService predictionService;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	RedisTemplateService redisTemplateService;

	@Autowired
	UserService userService;

	@Autowired
	AdUnitProperties adUnitProperties;

	@GetMapping("/match/question/{id}/{size}")
	public String questionPlay(@PathVariable("id") String id, @PathVariable("size") String sizes,
			HttpServletRequest request, ModelMap modelMap) {

		HttpSession session= request.getSession(true);
		String decoded = new String(Base64.decodeBase64(id));
		Long matchId = Long.valueOf(decoded);
		if (sizes == null) {
			return "redirect:/";
		}
		int size = Integer.valueOf(sizes);

		// if(CookieConfig.getCookies(request, "userId") == null) {
		// return "redirect:/intro";
		// }

		// String userId = String.valueOf(request.getSession().getAttribute("userId"));
		String userId = CookieConfig.getCookies(request, "userId");

		/* System.out.println("userId : " + userId); */

		List<QuestionPlainPojo> questionList = predictionService.getPredictionQuestion(matchId);

		MatchEventPlainPojo matchEvent = predictionService.getMatchList(matchId);
		if (matchEvent == null) {
			return "redirect:/";
		}
		String userAnsStats = null;
		QuestionPlainPojo questionPlainPojo = null;
		if (!CollectionUtils.isEmpty(questionList)) {
//			int sessSize = 0;
//			if (session.getAttribute(matchId + "_" + userId + "question_size") != null) {
//				sessSize = (int) session.getAttribute(matchId + "_" + userId + "question_size");
//			}
//			if (sessSize == size) {
				questionPlainPojo = questionList.get(size);
				
				userAnsStats = predictionService.getUserStatsByMatch(matchId, questionPlainPojo);
//
//			} else {
//				session.setAttribute(matchId + "_" + userId + "question_size", size);
//			}
			
			
			
		}

		if (questionPlainPojo == null) {
			return "redirect:/";
		}
		
		if (matchEvent.getEntryAmount() == 0 || matchEvent.getEntryAmount() == null) {
			String appName = CookieConfig.getCookies(request, "getSubDomain");
			predictionApiService.matchJoinningApiCall(userId, matchId, appName);
		}

		MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);

		String nextQuestData = predictionService.nextQuestionEncrpt(matchId, questionList.size() + 1, size,
				questionPlainPojo);
		modelMap.addAttribute("userAnswerStat", userAnsStats);
		modelMap.addAttribute("matchPojoList", matchPojo);

		modelMap.addAttribute("questionPlainPojo", questionPlainPojo);
		modelMap.addAttribute("questionSize", questionList.size());
		modelMap.addAttribute("matchEvent", matchEvent);
		modelMap.addAttribute("nextQuestion", nextQuestData);
		modelMap.addAttribute("size", size + 1);
		modelMap.addAttribute("userId",userId);
		
		String host = NetworkUtils.getSubDomain(request);
		String Pred_Popup_Proceed = adUnitProperties.getAdUnit(host, "Pred_Popup_Proceed");
		modelMap.addAttribute("Pred_Popup_Proceed", Pred_Popup_Proceed);

		return "predquestion/question";
	}

	@PostMapping("/match/questionPlay")
	public @ResponseBody Response questionPlayAjaxs(@RequestBody Data data, HttpServletRequest request,HttpSession session) {

		String dataArr[] = data.getData().split("-");

		String userId = CookieConfig.getCookies(request, "userId");

		Long matchId = Long.valueOf(dataArr[0]);
		String questionSize = dataArr[1];
		int size = Integer.valueOf(dataArr[2]);
		session.setAttribute(matchId + "_" + userId + "question_size", size+1);
		System.out.println("size xx  "+size);
		
		Long quesId = Long.valueOf(dataArr[3]);
		String givenAns = "";

		if (data.getAnswer().equals("optionA")) {
			givenAns = "A";
		}
		if (data.getAnswer().equals("optionB")) {
			givenAns = "B";
		}
		if (data.getAnswer().equals("optionC")) {
			givenAns = "C";
		}
		if (data.getAnswer().equals("optionD")) {
			givenAns = "D";
		}

		String appName = CookieConfig.getCookies(request, "getSubDomain");

		Response response = predictionApiService.predictionJoinApiCall(matchId, quesId, userId, givenAns, appName);

		return response;

	}

	@GetMapping("/match/score/{id}")
	public String score(@PathVariable("id") String id, HttpServletRequest request, ModelMap modelMap)
			throws JsonProcessingException {

		String decoded = new String(Base64.decodeBase64(id));
		Long matchId = Long.valueOf(decoded);
		ObjectMapper objectMapper = new ObjectMapper();
		String userId = CookieConfig.getCookies(request, "userId");

		MatchEventPlainPojo matchEvent = predictionService.getMatchList(matchId);
		/*
		 * System.out.println("at match score request mapping.." + matchEvent.getCoins()
		 * + "match entry amount " + matchEvent.getEntryAmount());
		 */

		if (matchEvent == null) {
			return "redirect:/";
		}

		if (userId == null) {
			try {
				List<MatchEventPlainPojo> matchList = null;
				String matchListStr = redisTemplateService.readFromRedis("matchEventen");

				if (!matchListStr.equals("")) {
					matchList = objectMapper.readValue(matchListStr,
							new TypeReference<ArrayList<MatchEventPlainPojo>>() {
							});
				}

				for (MatchEventPlainPojo po : matchList) {

					Long mId = po.getId();

					if (po.getEntryAmount() == -1) {
						po.setEntryAmount(Constants.WATCH_VIDEO_COIN);
					}

					Date startTime = po.getStartDate();
					long nn = startTime.getTime();
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(nn);
					String timeString = new SimpleDateFormat("d MMMM,hh:mm a").format(cal.getTime());
					po.setTimeStamp(timeString);

				}

				List<MatchEventPlainPojo> moreMatches = new ArrayList<>();
				int count = 0;
				Date date = new Date();
				for (MatchEventPlainPojo match : matchList) {

					if (match.getStartDate().after(date) && match.getId() != matchEvent.getId() && count < 2 && match.getSingleTeam() == false) {

						moreMatches.add(match);
						Long diffDate = userService.findDifference(match.getStartDate());
						match.setCountDownTimer(diffDate);
						count++;
					}

				}

				modelMap.addAttribute("matchList", moreMatches);
				modelMap.addAttribute("matchEvent", matchEvent);
				modelMap.addAttribute("userId", userId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

			Response response = predictionApiService.matchPreizeMatrixApi(matchId);

			if (response.getStatus() != 200) {
				return "redirect:/";
			}

			modelMap.addAttribute("prizMatrix", response.getMessage());

			modelMap.addAttribute("matchPojoList", matchEvent);

//			MatchEventPlainPojo matchEvent1 = predectionService.getMatchList(matchId);

			String matchStr = objectMapper.writeValueAsString(matchEvent);
			String matchsdsa = "success";

			try {
				redisTemplateService.writeHashValueToField("match_score_" + userId, "" + matchId, matchsdsa);
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				List<MatchEventPlainPojo> matchList = null;
				String matchListStr = redisTemplateService.readFromRedis("matchEventen");

				if (!matchListStr.equals("")) {
					matchList = objectMapper.readValue(matchListStr,
							new TypeReference<ArrayList<MatchEventPlainPojo>>() {
							});
				}

				Map<String, String> scoreCountString = redisTemplateService.readHashHgetAll("match_score_" + userId);

				for (MatchEventPlainPojo po : matchList) {

					Long mId = po.getId();

//					if(po.getPriceMoney() == 0) { if(!scoreCountString.containsKey(""+mId)) {
//						if(matchList.size()==2) { break; }else { matchList.add(po); } } }

					if (po.getEntryAmount() == -1) {
						po.setEntryAmount(Constants.WATCH_VIDEO_COIN);
					}
					String userJoin = redisTemplateService.readHashValueFromField("match_score_" + userId, "" + mId);

					if (userJoin.equals("success")) {
						po.setPlayed(userJoin);
					} else if(request.getSession().getAttribute(matchId + "_" + userId + "question_size") != null){
						po.setPlayed("pending");
					} else  {
						po.setPlayed("failure");
					}

					Date startTime = po.getStartDate();
					long nn = startTime.getTime();
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(nn);
					String timeString = new SimpleDateFormat("d MMMM,hh:mm a").format(cal.getTime());
					po.setTimeStamp(timeString);

				}
				String userWall = redisTemplateService.readHashValueFromField("userWallet", userId);
				if (userWall.equals("")) {
					return "redirect:/";
				}

				List<MatchEventPlainPojo> moreMatches = new ArrayList<>();
				int count = 0;
				Date date = new Date();
				for (MatchEventPlainPojo match : matchList) {
					if (match.getStartDate().after(date) && !match.getPlayed().equals("success")
							&& match.getId() != matchEvent.getId() && count < 2 && match.getSingleTeam() == false) {
						moreMatches.add(match);
						Long diffDate = userService.findDifference(match.getStartDate());
						match.setCountDownTimer(diffDate);
						count++;
					}
					if (count == 2)
						break;
				}
				moreMatches.forEach(n -> System.out.println(n.getName()));
//				modelMap.addAttribute("matchList", matchList.stream().limit(2).collect(Collectors.toList()));
				modelMap.addAttribute("matchList", moreMatches);
				WalletDto walletDto = userService.getWalletAmount(userId, userWall);
				modelMap.addAttribute("coinBalance", walletDto.getCoinBalance());
				modelMap.addAttribute("matchEvent", matchEvent);
				modelMap.addAttribute("userId", userId);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String host = NetworkUtils.getSubDomain(request);
		String Score_Screen_Feed = adUnitProperties.getAdUnit(host, "Score_Screen_Feed");
		modelMap.addAttribute("Score_Screen_Feed", Score_Screen_Feed);

		String Point_System_Page = adUnitProperties.getAdUnit(host, "Point_System_Page");
		modelMap.addAttribute("Point_System_Page", Point_System_Page);

		return "predquestion/score";
	}

}
