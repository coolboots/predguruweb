package com.coolboots.qureka.predGuru.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.Data;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.QuestionPlainPojo;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.user.api.AppUserApiService;
import com.coolboots.qureka.predGuru.user.api.WalletApiService;
import com.coolboots.qureka.predGuru.user.service.UserService;
import com.coolboots.qureka.predGuru.utils.AESCrypto;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.DynamicApiKeyConstant;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;

@Controller
public class JoiningController {

	@Autowired
	WalletApiService walletApiService;

	@Autowired
	UserService userService;

	@Autowired
	RedisTemplateService redisTemService;

	@Autowired
	PredictionService predictionService;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	AppUserApiService appUserApiService;

	@Autowired
	AdUnitProperties adUnitProperties;
	
	@GetMapping("/match/coinEntry")
	public String Joining(HttpServletRequest request, ModelMap modelMap) {

		Long matchId = Long.valueOf(request.getParameter("id"));
		String host = NetworkUtils.getSubDomain(request);
		String Start_Screen_Top = adUnitProperties.getAdUnit(host, "Start_Screen_Top");
		modelMap.addAttribute("Start_Screen_Top",Start_Screen_Top);
		try {

//			if(request.getSession().getAttribute("userId") == null) {
//				return "redirect:/";
//	   	 	}

			// String userId = String.valueOf(request.getSession().getAttribute("userId"));
			String userId = CookieConfig.getCookies(request, "userId");
			predictionApiService.userStatsAPiCall(matchId);

			if (userId == null) {

				String matchStr = redisTemService.readFromRedis("matchEventen");

				if (matchStr.equals("")) {
					return "redirect:/";
				}

				MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
				if (matchPojo.getEntryAmount() == -1) {
					matchPojo.setEntryAmount(Constants.WATCH_VIDEO_COIN);
				}

				String joinData = predictionService.getJoinEncrypt(matchId, matchPojo.getEntryAmount());

				modelMap.addAttribute("joinData", joinData);
				modelMap.addAttribute("matchPojoList", matchPojo);

				String matchIdStr = String.valueOf(matchPojo.getId());
				String encoded = new String(Base64.encodeBase64(matchIdStr.getBytes()));
				modelMap.addAttribute("matchIdStrEncrpt", encoded);

				List<QuestionPlainPojo> noOfQuestion = predictionService.getPredictionQuestion(matchId);
				modelMap.addAttribute("noOfQuestion", noOfQuestion.size());
				Date dd = new Date();
				long msec = dd.getTime();
				modelMap.addAttribute("newDate",msec);

			} else {
				int size =0;
				if(request.getSession().getAttribute(matchId + "_" + userId + "question_size")!= null) {
					size=(int)request.getSession().getAttribute(matchId + "_" + userId + "question_size");
				}

				String userWall = redisTemService.readHashValueFromField("userWallet", userId);
				if (userWall.equals("")) {
					return "redirect:/";
				}

				String matchStr = redisTemService.readFromRedis("matchEventen");

				if (matchStr.equals("")) {
					return "redirect:/";
				}

				WalletDto walletDto = userService.getWalletAmount(userId, userWall);
				modelMap.addAttribute("coinBalance", walletDto.getCoinBalance());

				MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
				if (matchPojo.getEntryAmount() == -1) {
					matchPojo.setEntryAmount(Constants.WATCH_VIDEO_COIN);
				}

				String joinData = predictionService.getJoinEncrypt(matchId, matchPojo.getEntryAmount());

				modelMap.addAttribute("joinData", joinData);
				modelMap.addAttribute("matchPojoList", matchPojo);
				modelMap.addAttribute("size",size);
				String matchIdStr = String.valueOf(matchPojo.getId());
				String encoded = new String(Base64.encodeBase64(matchIdStr.getBytes()));
				modelMap.addAttribute("matchIdStrEncrpt", encoded);

				List<QuestionPlainPojo> noOfQuestion = predictionService.getPredictionQuestion(matchId);
				modelMap.addAttribute("noOfQuestion", noOfQuestion.size());
				
				modelMap.addAttribute("countDownTimer",matchPojo.getCountDownTimer());
				
				Date dd = new Date();
				long msec = dd.getTime();
				modelMap.addAttribute("newDate",msec);
				
				Long diffDate = userService.findDifference(matchPojo.getStartDate());
				matchPojo.setCountDownTimer(diffDate);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "joining/coinentry";
	}

	@PostMapping("/pMinusCoin")
	public @ResponseBody Response callMinusCoin(@RequestBody Data data, HttpServletRequest request, ModelMap modelMap) {

		Response responseObj = new Response();
		responseObj.setStatus(400);

		String hashkey = DynamicApiKeyConstant.PREDICTION_JOIN_ENCRYPT_VALUE;
		String decryptedCipherText;
		String userId = CookieConfig.getCookies(request, "userId");

		try {
			decryptedCipherText = AESCrypto.decryptUtils(hashkey, data.getData());
			System.out.println(decryptedCipherText);
			String dataArr[] = decryptedCipherText.split("-");

			if (dataArr[0] == null && dataArr[0].equals("") && dataArr[1] == null && dataArr[1].equals("")) {
				return responseObj;
			}

			Long matchId = Long.valueOf(dataArr[0]);
			int entryFees = Integer.valueOf(dataArr[1]);

			System.out.println(matchId + " -----------------" + entryFees);

			MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
			if (matchPojo.getEntryAmount() == -1) {
				matchPojo.setEntryAmount(Constants.WATCH_VIDEO_COIN);
			}
			if (matchPojo.getEntryAmount() != entryFees) {
				return responseObj;
			}

			String appName = CookieConfig.getCookies(request, "getSubDomain");

			System.out.println(appName);

			Response preJoinning = predictionApiService.matchJoinningApiCall(userId, matchId, appName);

			System.out.println("preJoinning : " + preJoinning);

			if (preJoinning.getStatus() == 200) {
				Response response = appUserApiService.minusCoinApiCall(userId, matchId, entryFees, "predection",
						appName);
				if (response.getStatus() == 200 || response.getMessage().equals("success")) {
					Response walletResponse = walletApiService.wallet(userId);
					if (walletResponse.getStatus() == 200) {
						redisTemService.deleteValueFromHash("userWallet", userId);
						userService.setUserWalletInRedis(walletResponse, userId);
					}
				}
				responseObj.setStatus(200);

			}
			if (preJoinning.getStatus() == 208 || preJoinning.getMessage().equals("false")) {
				responseObj.setStatus(208);
			}
			return responseObj;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseObj;
	}

	@GetMapping("/match/freeentry")
	public String freeJoining(HttpServletRequest request, ModelMap modelMap) {

		Long matchId = Long.valueOf(request.getParameter("id"));

		try {

			/*
			 * if(request.getSession().getAttribute("userId") == null) { return
			 * "redirect:/intro"; }
			 */

			// String userId = String.valueOf(request.getSession().getAttribute("userId"));
			String userId = CookieConfig.getCookies(request, "userId");
			predictionApiService.userStatsAPiCall(matchId);
			String host = NetworkUtils.getSubDomain(request);
			String Start_Screen_Top = adUnitProperties.getAdUnit(host, "Start_Screen_Top");
			modelMap.addAttribute("Start_Screen_Top",Start_Screen_Top);
			if (userId == null) {

				String matchStr = redisTemService.readFromRedis("matchEventen");
				if (matchStr.equals("")) {
					return "redirect:/";
				}

				MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
				modelMap.addAttribute("matchPojoList", matchPojo);
				
				modelMap.addAttribute("countDownTimer",matchPojo.getCountDownTimer());
				
				String matchIdStr = String.valueOf(matchPojo.getId());
				String encoded = new String(Base64.encodeBase64(matchIdStr.getBytes()));
				modelMap.addAttribute("matchIdStrEncrpt", encoded);

				List<QuestionPlainPojo> noOfQuestion = predictionService.getPredictionQuestion(matchId);
				modelMap.addAttribute("noOfQuestion", noOfQuestion.size());
				modelMap.addAttribute("userId", userId);
				Date dd = new Date();
				long msec = dd.getTime();
				modelMap.addAttribute("newDate",msec);

			} else {

				String userWall = redisTemService.readHashValueFromField("userWallet", userId);
				if (userWall.equals("")) {
					return "redirect:/";
				}

				String matchStr = redisTemService.readFromRedis("matchEventen");

				if (matchStr.equals("")) {
					return "redirect:/";
				}

				WalletDto walletDto = userService.getWalletAmount(userId, userWall);
				modelMap.addAttribute("coinBalance", walletDto.getCoinBalance());

				MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
				modelMap.addAttribute("matchPojoList", matchPojo);
				
				String matchIdStr = String.valueOf(matchPojo.getId());
				String encoded = new String(Base64.encodeBase64(matchIdStr.getBytes()));
				modelMap.addAttribute("matchIdStrEncrpt", encoded);

				List<QuestionPlainPojo> noOfQuestion = predictionService.getPredictionQuestion(matchId);
				modelMap.addAttribute("noOfQuestion", noOfQuestion.size());
				modelMap.addAttribute("userId", userId);
				
				modelMap.addAttribute("countDownTimer",matchPojo.getCountDownTimer());
				
				Date dd = new Date();
				long msec = dd.getTime();
				modelMap.addAttribute("newDate",msec);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "joining/freeentry";
	}

	@GetMapping("predictionRules")
	public String getPredictionRules(HttpServletRequest request, ModelMap modelMap) {
		return "rules/predictionrules";
	}

}
