package com.coolboots.qureka.predGuru.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.dto.UserRegistrationDto;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.SampleMatchPojo;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.user.api.WalletApiService;
import com.coolboots.qureka.predGuru.user.service.UserService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;

@Controller
@RequestMapping("/")
public class DashboardController {

	@Autowired
	UserService userService;

	@Autowired
	WalletApiService walletApiService;

	@Autowired
	RedisTemplateService redisTemService;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	PredictionService predictionService;

	@Autowired
	AdUnitProperties adUnitProperties;

	@GetMapping
	public String dashboard(HttpServletRequest request, ModelMap modelMap, HttpServletResponse htResponse,
			UserRegistrationDto userDto) {

		String userId = CookieConfig.getCookies(request, "userId");
		String host = NetworkUtils.getSubDomain(request);

		String Pred_Feed = adUnitProperties.getAdUnit(host, "Pred_Feed");
		modelMap.addAttribute("Pred_Feed", Pred_Feed);

		String Point_System_Page = adUnitProperties.getAdUnit(host, "Point_System_Page");
		modelMap.addAttribute("Point_System_Page", Point_System_Page);

		if (userId == null) {

			if (request.getSession().getAttribute("redirectSuccess") == null) {

				SampleMatchPojo sampleMatchPojo = predictionService.getSampleQuestions();
				String login = "no";
				if (CookieConfig.getCookies(request, "userId") != null) {
					login = "yes";
				}

				modelMap.addAttribute("sampleMatch", sampleMatchPojo);

				modelMap.addAttribute("login", login);
				request.getSession().removeAttribute("redirectSuccess");

				String Intro_Page = adUnitProperties.getAdUnit(host, "Intro_Page");
				modelMap.addAttribute("Intro_Page", Intro_Page);

				return "index";
			}

			List<MatchEventPlainPojo> matchList = null;

			matchList = predictionService.getMatchList(request);

			List<MatchEventPlainPojo> sortedMatches = matchList.stream()
//					.sorted(Comparator.comparing(MatchEventPlainPojo::getCoins).reversed())				
					.sorted(Comparator.comparing(MatchEventPlainPojo::getStartDate)).collect(Collectors.toList());

			/* get highest entry of coins from the list */
			MatchEventPlainPojo matchHavingMaxCoins = sortedMatches.stream()
					.max(Comparator.comparing(MatchEventPlainPojo::getCoins)).orElseThrow(NoSuchElementException::new);

			matchHavingMaxCoins.setCountDownTimer(userService.findDifference(matchHavingMaxCoins.getStartDate()));

			sortedMatches.remove(matchHavingMaxCoins);

			for (MatchEventPlainPojo po : matchList) {
				Long diffDate = userService.findDifference(po.getStartDate());
				po.setCountDownTimer(diffDate);
			}

			modelMap.addAttribute("coinBalance", 0);
			modelMap.addAttribute("userId", userId);
			modelMap.addAttribute("matchHavingMaxCoins", matchHavingMaxCoins);
			modelMap.addAttribute("matchList", sortedMatches.stream().filter(m -> m.getSingleTeam() == false).collect(Collectors.toList()));
			request.getSession().removeAttribute("userTag");
			Date dd = new Date();
			long msec = dd.getTime();
			modelMap.addAttribute("newDate",msec);

			String Sticky_Top = adUnitProperties.getAdUnit(host, "Sticky_Top");
			modelMap.addAttribute("Sticky_Top", Sticky_Top);
			

		} else {
			
			String userWall = redisTemService.readHashValueFromField("userWallet", userId);

			if (userWall.equals("")) {
				Response walletResponse = walletApiService.wallet(userId);
				userService.setUserWalletInRedis(walletResponse, userId);
				userWall = redisTemService.readHashValueFromField("userWallet", userId);
			}

			Double walletBalanec = 0.0;
			Double referralBalance = 0.0;
			int coinBalance = 0;

			try {

				WalletDto walletDto = userService.getWalletAmount(userId, userWall);
				if (walletDto != null) {
					if (walletDto.getWalletBalanec() != null) {
						walletBalanec = (double) Constants.round(walletDto.getWalletBalanec(), 2);
					}
					if (walletDto.getReferralBalance() != null) {
						referralBalance = (double) Constants.round(walletDto.getReferralBalance(), 2);
					}
					if (walletDto.getCoinBalance() > 0) {
						coinBalance = walletDto.getCoinBalance();
					}
				}

				String userTag = String.valueOf(request.getSession().getAttribute("userTag"));
				String dailyBonus = "";
				if (request.getSession().getAttribute("userTag") == null) {

					/* user get daily bonus after 24 hours.. */
					// if (request.getSession().getAttribute("current user added date") != null) {

					// Date UserAddedDate = (Date) request.getSession().getAttribute("current user
					// added date");

					// Date currentDate = new Date();

					// long difference_In_Time = currentDate.getTime() - UserAddedDate.getTime();

					// long diffInHours = TimeUnit.MILLISECONDS.toHours(difference_In_Time);

					// if (diffInHours >= 24) {
					dailyBonus = userService.userDailyBonus(userId, coinBalance, htResponse, request);
					// }

					// }

					userService.userWallet(userId);
					userWall = redisTemService.readHashValueFromField("userWallet", userId);
					walletDto = userService.getWalletAmount(userId, userWall);
					if (walletDto != null) {
						if (walletDto.getWalletBalanec() != null) {
							walletBalanec = (double) Constants.round(walletDto.getWalletBalanec(), 2);
						}
						if (walletDto.getReferralBalance() != null) {
							referralBalance = (double) walletDto.getReferralBalance();
						}
						if (walletDto.getCoinBalance() > 0) {
							coinBalance = walletDto.getCoinBalance();
						}
					}
				}

				List<MatchEventPlainPojo> matchList = null;

				matchList = predictionService.getMatchList(request);

				List<MatchEventPlainPojo> sortedMatches = matchList.stream()
//						.sorted(Comparator.comparing(MatchEventPlainPojo::getCoins).reversed())
						.sorted(Comparator.comparing(MatchEventPlainPojo::getStartDate)).collect(Collectors.toList());

				/* get highest entry of coins from the list */
				MatchEventPlainPojo matchHavingMaxCoins = sortedMatches.stream()
						.max(Comparator.comparing(MatchEventPlainPojo::getCoins))
						.orElseThrow(NoSuchElementException::new);

				matchHavingMaxCoins.setCountDownTimer(userService.findDifference(matchHavingMaxCoins.getStartDate()));

				sortedMatches.remove(matchHavingMaxCoins);

				for (MatchEventPlainPojo po : matchList) {
					Long diffDate = userService.findDifference(po.getStartDate());
					po.setCountDownTimer(diffDate);
				}

				String Sticky_Top = adUnitProperties.getAdUnit(host, "Sticky_Top");
				modelMap.addAttribute("Sticky_Top", Sticky_Top);

				String Welcome_Popup = adUnitProperties.getAdUnit(host, "Welcome_Popup");
				modelMap.addAttribute("Welcome_Popup", Welcome_Popup);
				modelMap.addAttribute("welcomedailyBonus", userTag);
				modelMap.addAttribute("dailyBonus", dailyBonus);
				modelMap.addAttribute("coinBalance", coinBalance);
				modelMap.addAttribute("userId", userId);
				modelMap.addAttribute("matchHavingMaxCoins", matchHavingMaxCoins);
				modelMap.addAttribute("matchList", sortedMatches.stream().filter(m -> m.getSingleTeam() == false).collect(Collectors.toList()));
				String loginType = CookieConfig.getCookies(request, "loginType");
//				System.out.println("loginType : " + loginType);
				modelMap.addAttribute("loginType", loginType);
				request.getSession().removeAttribute("userTag");
				
				Date dd = new Date();
				long msec = dd.getTime();
				modelMap.addAttribute("newDate",msec);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return "dashboard/index";
	}
}
