package com.coolboots.qureka.predGuru.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.UserRank;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.service.RecentWinnerService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/")
public class RecentWinnerController {

	@Autowired
	RedisTemplateService redisTemplateService;

	@Autowired
	RecentWinnerService recentWinners;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	PredictionService predictionService;

	@Autowired
	AdUnitProperties adUnitProperties;

	@GetMapping("recentwinners")
	public String getQuizPrediction(HttpServletRequest request, ModelMap modelMap) {

		try {

//			if(CookieConfig.getCookies(request, "userId") == null) {
//				return "redirect:/intro";
//		 	}

			String matchListStr = redisTemplateService.readFromRedis("matchEventen");
			if (matchListStr.equals("")) {
				return "redirect:/";
			}

			String userId = CookieConfig.getCookies(request, "userId");
			String profileImage = CookieConfig.getCookies(request, "profileImage");

			ObjectMapper objectMapper = new ObjectMapper();

			List<MatchEventPlainPojo> matchList = null;

			matchList = objectMapper.readValue(matchListStr, new TypeReference<ArrayList<MatchEventPlainPojo>>() {
			});

			for (MatchEventPlainPojo pp : matchList) {

				if (pp.getEntryAmount() == -1) {
					pp.setEntryAmount(Constants.WATCH_VIDEO_COIN);
				}
			}

			modelMap.addAttribute("profileImage", profileImage);
			modelMap.addAttribute("matchList",
					matchList.stream().filter(u -> u.getMatchCompleted() != null && u.getResultStatus() == 1)
							.sorted(Comparator.comparing(MatchEventPlainPojo::getStartDate).reversed())
							.collect(Collectors.toList()));

			String host = NetworkUtils.getSubDomain(request);

			String Recent_Winners_Listing = adUnitProperties.getAdUnit(host, "Recent_Winners_Listing");
			modelMap.addAttribute("Recent_Winners_Listing",Recent_Winners_Listing);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "winner/winners";
	}

	@GetMapping("match/winner")
	public String recentPredictionWinner(HttpServletRequest request, ModelMap modelMap) {

		try {

//			if(CookieConfig.getCookies(request, "userId") == null) {
//				return "redirect:/intro";
//			}

			String userId = CookieConfig.getCookies(request, "userId");
			Long matchId = Long.valueOf(request.getParameter("id"));
			String profileImage = CookieConfig.getCookies(request, "profileImage");

			List<UserRank> matchWinnerList = null;
			MatchEventPlainPojo matchList = predictionService.getMatchList(matchId);
			modelMap.addAttribute("matchList", matchList);
			Response response = recentWinners.recentPredictionWinnerApiCall(matchId);
			if (response.getStatus() == 200) {
				matchWinnerList = (List<UserRank>) response.getObject();
			}

			UserRank filterWinner = matchWinnerList.stream()
					.filter(u -> u.getUserPojo().getId() != null && u.getUserPojo().getId().equals(userId)).findAny()
					.orElse(null);
			if (filterWinner != null) {
				modelMap.addAttribute("coin", filterWinner.getCoins());
				modelMap.addAttribute("money", filterWinner.getMoney());
				modelMap.addAttribute("profileImage", profileImage);
			} else {
				modelMap.addAttribute("coin", 0);
				modelMap.addAttribute("money", 0);
			}

			modelMap.addAttribute("userId", userId);
			modelMap.addAttribute("profileImage", profileImage);
			modelMap.addAttribute("matchWinnerList", matchWinnerList.stream().limit(500).collect(Collectors.toList()));

			String host = NetworkUtils.getSubDomain(request);

			String Check_Winner_List = adUnitProperties.getAdUnit(host, "Check_Winner_List");
			modelMap.addAttribute("Check_Winner_List",Check_Winner_List);


		} catch (Exception e) {
			e.printStackTrace();
		}
		return "winner/winnerdetails";
	}

}
