package com.coolboots.qureka.predGuru.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.PrizeMatrix;
import com.coolboots.qureka.predGuru.service.PredictionService;
import com.coolboots.qureka.predGuru.utils.AdUnitProperties;
import com.coolboots.qureka.predGuru.utils.NetworkUtils;

@Controller
public class PrizeMatrixController {
	
	@Autowired
	PredictionApiService predApiService;
	
	@Autowired
	PredictionService predictionService;
	
	@Autowired
	AdUnitProperties adUnitProperties;
	
	@GetMapping("/match/rank")
	public String rank(HttpServletRequest request, ModelMap modelMap) {
		
		
//		if(CookieConfig.getCookies(request, "userId") == null) {
//			return "redirect:/intro";
//	 	}
		
		Long matchId	=	 Long.valueOf(request.getParameter("id"));
		

		Response response = predApiService.matchPreizeMatrixApi(matchId);
		
		List<PrizeMatrix> prizeMatrixs = null;
		
		if(response != null) {
			prizeMatrixs = (List<PrizeMatrix>) response.getObject();
		}
		
    	MatchEventPlainPojo matchPojo = predictionService.getMatchList(matchId);
		
		modelMap.addAttribute("prizeMatrixList", prizeMatrixs);
		modelMap.addAttribute("match", matchPojo);
				
		String host = NetworkUtils.getSubDomain(request);
		String Popup_Rank_Breakup = adUnitProperties.getAdUnit(host, "Popup_Rank_Breakup");
		modelMap.addAttribute("Popup_Rank_Breakup",Popup_Rank_Breakup);
		
		
		return "rank/popup";
	}
}
