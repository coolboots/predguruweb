package com.coolboots.qureka.predGuru.model;

import java.util.Date;

import com.coolboots.qureka.predGuru.model.enums.Answer;

public class Question {

    
    private Long id;
    private QuestionSet questionSet;
    private Long matchId;
    private String question;   
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String funFact;
    private Long mark;
    private Integer sequence;

    private Answer answer = Answer.SELECT_ANSWER;
    private Date createdOn;
    private Date updateOn;
    private Date startOn;
    private Boolean live;
    private Date liveOn;
    private Boolean isActive;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public QuestionSet getQuestionSet() {
		return questionSet;
	}

	public void setQuestionSet(QuestionSet questionSet) {
		this.questionSet = questionSet;
	}


	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    
    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }
    
    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
    
    public String getFunFact() {
		return funFact;
	}

	public void setFunFact(String funFact) {
		this.funFact = funFact;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }
    
    
    public Date getStartOn() {
        return startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    public Boolean getLive() {
        return live;
    }

    public void setLive(Boolean live) {
        this.live = live;
    }
    
    public Date getLiveOn() {
		return liveOn;
	}

	public void setLiveOn(Date liveOn) {
		this.liveOn = liveOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

    public Long getMark() {
        return mark;
    }

    public void setMark(Long mark) {
        this.mark = mark;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		result = prime * result + ((matchId == null) ? 0 : matchId.hashCode());
		result = prime * result + ((questionSet == null) ? 0 : questionSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (questionSet == null) {
			if (other.questionSet != null)
				return false;
		} else if (!questionSet.equals(other.questionSet))
			return false;
		return true;
	}

	
	
	
	
	@Override
	public String toString() {
		return "Question [id=" + id + ", matchId=" + matchId + ", question=" + question
				+ ", optionA=" + optionA + ", optionB=" + optionB + ", optionC=" + optionC + ", optionD=" + optionD
				+ ", answer=" + answer + ", createdOn=" + createdOn + ", updateOn=" + updateOn + ", startOn=" + startOn
				+ ", live=" + live + ", liveOn=" + liveOn + ", isActive=" + isActive + "]";
	}

		
	
	@Override
	public Question clone() throws CloneNotSupportedException {
		
		return (Question) super.clone();
	}
}
