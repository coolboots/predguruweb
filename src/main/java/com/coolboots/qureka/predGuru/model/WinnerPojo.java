package com.coolboots.qureka.predGuru.model;

import java.util.List;

public class WinnerPojo {

	private Integer users;
	
	private List<QuizWinnerPojo> winners;
	
	public Integer getUsers() {
		return users;
	}
	public void setUsers(Integer users) {
		this.users = users;
	}
	
	public List<QuizWinnerPojo> getWinners() {
		return winners;
	}
	public void setWinners(List<QuizWinnerPojo> winners) {
		this.winners = winners;
	}
	
}
