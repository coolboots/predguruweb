package com.coolboots.qureka.predGuru.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


public class QuestionSet {

    private Long id;
    private MatchEvent matchEvent;
    private String description;
    private List<Question> questions;
    private Date addedOn;
    private Date updateOn;
    private Date startOn;
    private Date endOn;
    private Boolean isActive;
    private Boolean isVideo;
    private String videoUrl;
    private String videoLength;
    private MultipartFile videoFile;
    private Boolean isVideoCorrect;
    private Boolean isPreMatch;
    private Boolean isLast;
    private Date nextSetTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MatchEvent getMatchEvent() {
        return matchEvent;
    }

    public void setMatchEvent(MatchEvent matchEvent) {
        this.matchEvent = matchEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<Question> getQuestions() {

        return questions;
    }

    public List<Question> getActiveQuestions() {

        if (!CollectionUtils.isEmpty(questions)) {
            try {
                return questions.stream().filter(q -> (true == q.getIsActive())).collect(Collectors.toList());
            } catch (Exception e) {
                e.printStackTrace();
                return new ArrayList<>();
            }
        }

        return questions;
    }


    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public Date getStartOn() {
        return startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    public Date getEndOn() {
        return endOn;
    }

    public void setEndOn(Date endOn) {
        this.endOn = endOn;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsVideo() {
        return isVideo;
    }

    public void setIsVideo(Boolean isVideo) {
        this.isVideo = isVideo;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }

    public MultipartFile getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(MultipartFile videoFile) {
        this.videoFile = videoFile;
    }

    public Boolean getIsVideoCorrect() {
        return isVideoCorrect;
    }

    public void setIsVideoCorrect(Boolean isVideoCorrect) {
        this.isVideoCorrect = isVideoCorrect;
    }

    public Boolean getIsPreMatch() {
        return isPreMatch;
    }

    public void setIsPreMatch(Boolean isPreMatch) {
        this.isPreMatch = isPreMatch;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(Boolean isLast) {
        this.isLast = isLast;
    }

    public Date getNextSetTime() {
        return nextSetTime;
    }

    public void setNextSetTime(Date nextSetTime) {
        this.nextSetTime = nextSetTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
//		result = prime * result + ((match == null) ? 0 : match.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        QuestionSet other = (QuestionSet) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "QuestionSet [id=" + id + ", description=" + description + ", questions="
                + questions + ", addedOn=" + addedOn + ", updateOn=" + updateOn + ", startOn=" + startOn + ", endOn="
                + endOn + ", isActive=" + isActive + ", videoUrl=" + videoUrl + ", videoLength=" + videoLength
                + ", videoFile=" + videoFile + ", isVideoCorrect=" + isVideoCorrect + ", isPreMatch=" + isPreMatch
                + ", isLast=" + isLast + ", nextSetTime=" + nextSetTime + "]";
    }


    

    @Override
    public QuestionSet clone() throws CloneNotSupportedException {
        QuestionSet cloneObj = (QuestionSet) super.clone();
        List<Question> cloneQuestion = new ArrayList<>(questions.size());

        for (Question question : this.questions) cloneQuestion.add((Question) question.clone());

        cloneObj.setQuestions(cloneQuestion);

        return cloneObj;
    }

}
