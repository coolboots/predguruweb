package com.coolboots.qureka.predGuru.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

public class QuestionSetPlainPojo {

    private Long id;
    private String matchEvent;
    private String description;
    private String videoUrl;
    private String videoLength;
    private Boolean isVideoCorrect;
    private Boolean isPreMatch;
    private Boolean isLast;
    private Date nextSetTime;
    private Date addedOn;
    private Date updateOn;
    private Date startOn;
    private Date endOn;
    private Boolean isActive;
    private Boolean isVideo;

    private List<QuestionPlainPojo> questions;

    public QuestionSetPlainPojo(){}

    /*public QuestionSetPlainPojo(QuestionSet questionSet) {
        this.id = questionSet.getId();
        this.matchEvent = questionSet.getMatchEvent().getId().toString();
        this.description = questionSet.getDescription();
        this.videoUrl = questionSet.getVideoUrl();
        this.videoLength = questionSet.getVideoLength();
        this.isVideoCorrect = questionSet.getIsVideoCorrect();
        this.isPreMatch = questionSet.getIsPreMatch();
        this.isLast = questionSet.getIsLast();
        this.nextSetTime = questionSet.getNextSetTime();
        this.addedOn = questionSet.getAddedOn();
        this.updateOn = questionSet.getUpdateOn();
        this.startOn = questionSet.getStartOn();
        this.endOn = questionSet.getEndOn();
        this.isActive = questionSet.getIsActive();
        this.isVideo = questionSet.getIsVideo();
    }*/



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatchEvent() {
        return matchEvent;
    }

    public void setMatchEvent(String matchEvent) {
        this.matchEvent = matchEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuestionPlainPojo> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionPlainPojo> questions) {
        this.questions = questions;
    }

    @JsonProperty("added_on")
    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    @JsonProperty("update_on")
    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    @JsonProperty("start_on")
    @DateTimeFormat(pattern = "dd-MM-yyyy hh:mm a")
    public Date getStartOn() {
        return startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    @JsonProperty("end_on")
    @DateTimeFormat(pattern = "dd-MM-yyyy hh:mm a")
    public Date getEndOn() {
        return endOn;
    }

    public void setEndOn(Date endOn) {
        this.endOn = endOn;
    }

    @JsonProperty("is_active")
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }


    @JsonProperty("is_video")
    public Boolean getIsVideo() {
        return isVideo;
    }

    public void setIsVideo(Boolean isVideo) {
        this.isVideo = isVideo;
    }


    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }


    public Boolean getIsVideoCorrect() {
        return isVideoCorrect;
    }

    public void setIsVideoCorrect(Boolean isVideoCorrect) {
        this.isVideoCorrect = isVideoCorrect;
    }


    public Boolean getIsPreMatch() {
        return isPreMatch;
    }

    public void setIsPreMatch(Boolean isPreMatch) {
        this.isPreMatch = isPreMatch;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(Boolean isLast) {
        this.isLast = isLast;
    }

    public Date getNextSetTime() {
        return nextSetTime;
    }

    public void setNextSetTime(Date nextSetTime) {
        this.nextSetTime = nextSetTime;
    }

	@Override
	public String toString() {
		return "QuestionSetPlainPojo [id=" + id + ", matchEvent=" + matchEvent + ", description=" + description
				+ ", videoUrl=" + videoUrl + ", videoLength=" + videoLength + ", isVideoCorrect=" + isVideoCorrect
				+ ", isPreMatch=" + isPreMatch + ", isLast=" + isLast + ", nextSetTime=" + nextSetTime + ", addedOn="
				+ addedOn + ", updateOn=" + updateOn + ", startOn=" + startOn + ", endOn=" + endOn + ", isActive="
				+ isActive + ", isVideo=" + isVideo + ", questions=" + questions + "]";
	}
    
    
}
