package com.coolboots.qureka.predGuru.model;

import java.util.Date;
import java.util.List;

import com.coolboots.qureka.predGuru.model.enums.MatchType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchEventPlainPojo {

	private Long id;
	private String name;
	private TeamPlainPojo teamA;
	private TeamPlainPojo teamB;
	private Date startDate;
	private Date addedOn;
	private Date UpdatedOn;
	private Boolean isActive;
	private MatchType matchType = MatchType.T20;
	private Integer day;
	private Integer session;
	private Integer innings;
	private String host;
	private String venue;
	private Boolean isVideoLive;
	private Long priceMoney;
	private String description;
	private Boolean preMatchExist;
	private Boolean matchCompleted;
	private Boolean prizeMatrixExist;
	private Long userCount;
	private Integer resultStatus = 0;
	private Long entryAmount;
	private Long coins;
	private List<QuestionSetPlainPojo> questionSets;
	private Date alarmTime;
	private Boolean isAlarm;
	private boolean singleTeam;
	private String played;
	private Date countDownDate;
	private String timeStamp;
	private Long countDownTimer;

	public Long getCountDownTimer() {
		return countDownTimer;
	}

	public void setCountDownTimer(Long countDownTimer) {
		this.countDownTimer = countDownTimer;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Date getCountDownDate() {
		return countDownDate;
	}

	public void setCountDownDate(Date countDownDate) {
		this.countDownDate = countDownDate;
	}

	public void setPlayed(String played) {
		this.played = played;
	}

	public String getPlayed() {
		return played;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Date getUpdatedOn() {
		return UpdatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		UpdatedOn = updatedOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public MatchType getMatchType() {
		return matchType;
	}

	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getSession() {
		return session;
	}

	public void setSession(Integer session) {
		this.session = session;
	}

	public Integer getInnings() {
		return innings;
	}

	public void setInnings(Integer innings) {
		this.innings = innings;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Long getPriceMoney() {
		return priceMoney;
	}

	public void setPriceMoney(Long priceMoney) {
		this.priceMoney = priceMoney;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getPreMatchExist() {
		return preMatchExist;
	}

	public void setPreMatchExist(Boolean preMatchExist) {
		this.preMatchExist = preMatchExist;
	}

	public Boolean getMatchCompleted() {
		return matchCompleted;
	}

	public void setMatchCompleted(Boolean matchCompleted) {
		this.matchCompleted = matchCompleted;
	}

	public Boolean getPrizeMatrixExist() {
		return prizeMatrixExist;
	}

	public void setPrizeMatrixExist(Boolean prizeMatrixExist) {
		this.prizeMatrixExist = prizeMatrixExist;
	}

	@JsonProperty("user_count")
	public Long getUserCount() {
		return userCount;
	}

	public void setUserCount(Long userCount) {
		this.userCount = userCount;
	}

	public Integer getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(Integer resultStatus) {
		this.resultStatus = resultStatus;
	}

	public Long getEntryAmount() {
		return entryAmount;
	}

	public void setEntryAmount(Long entryAmount) {
		this.entryAmount = entryAmount;
	}

	public TeamPlainPojo getTeamA() {
		return teamA;
	}

	public void setTeamA(TeamPlainPojo teamA) {
		this.teamA = teamA;
	}

	public TeamPlainPojo getTeamB() {
		return teamB;
	}

	public void setTeamB(TeamPlainPojo teamB) {
		this.teamB = teamB;
	}

	public Long getCoins() {
		return coins;
	}

	public void setCoins(Long coins) {
		this.coins = coins;
	}

	public Date getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(Date alarmTime) {
		this.alarmTime = alarmTime;
	}

	public Boolean getIsAlarm() {
		return isAlarm;
	}

	public void setIsAlarm(Boolean isAlarm) {
		this.isAlarm = isAlarm;
	}

	@JsonProperty("singleTeam")
	public boolean getSingleTeam() {
		return singleTeam;
	}

	public void setSingleTeam(boolean singleTeam) {
		this.singleTeam = singleTeam;
	}

	@JsonProperty("questionSets")
	public List<QuestionSetPlainPojo> getQuestionSetPojos() {
		return questionSets;
	}

	public void setQuestionSetPojos(List<QuestionSetPlainPojo> questionSetPojos) {
		this.questionSets = questionSetPojos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchEventPlainPojo other = (MatchEventPlainPojo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MatchEventPlainPojo [id=" + id + ", name=" + name + ", teamA=" + teamA + ", teamB=" + teamB
				+ ", startDate=" + startDate + ", addedOn=" + addedOn + ", UpdatedOn=" + UpdatedOn + ", isActive="
				+ isActive + ", matchType=" + matchType + ", day=" + day + ", session=" + session + ", innings="
				+ innings + ", host=" + host + ", venue=" + venue + ", isVideoLive=" + isVideoLive + ", priceMoney="
				+ priceMoney + ", description=" + description + ", preMatchExist=" + preMatchExist + ", matchCompleted="
				+ matchCompleted + ", prizeMatrixExist=" + prizeMatrixExist + ", userCount=" + userCount
				+ ", resultStatus=" + resultStatus + ", entryAmount=" + entryAmount + ", coins=" + coins
				+ ", questionSets=" + questionSets + ", alarmTime=" + alarmTime + ", isAlarm=" + isAlarm
				+ ", singleTeam=" + singleTeam + ", played=" + played + ", countDownDate=" + countDownDate
				+ ", timeStamp=" + timeStamp + "]";
	}

}
