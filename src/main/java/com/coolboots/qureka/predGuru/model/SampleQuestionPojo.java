package com.coolboots.qureka.predGuru.model;

public class SampleQuestionPojo {

	private Long questionId;
	private String question;
	private String optionA;
	private String optionB;
	private String optionC;
	private String optionD;
	private String funFact;

	public String getFunFact() {
		return funFact;
	}

	public void setFunFact(String funFact) {
		this.funFact = funFact;
	}

	private String stats;
//	private Integer statA;
//	private Integer statB;
//	private Integer statC;
//	private Integer statD;
//	private Integer totalUsers;

	public SampleQuestionPojo() {

	}

	public SampleQuestionPojo(QuestionPlainPojo question) {
		this.questionId = question.getId();
		this.question = question.getQuestion();
		this.optionA = question.getOptionA();
		this.optionB = question.getOptionB();
		this.optionC = question.getOptionC();
		this.optionD = question.getOptionD();
		this.funFact = question.getFunFact();
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getOptionA() {
		return optionA;
	}

	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}

	public String getOptionB() {
		return optionB;
	}

	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}

	public String getOptionC() {
		return optionC;
	}

	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}

	public String getOptionD() {
		return optionD;
	}

	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}

	public String getStats() {
		return stats;
	}

	public void setStats(String stats) {
		this.stats = stats;
	}

//	public Integer getStatA() {
//		return statA;
//	}
//	public void setStatA(Integer statA) {
//		this.statA = statA;
//	}
//	public Integer getStatB() {
//		return statB;
//	}
//	public void setStatB(Integer statB) {
//		this.statB = statB;
//	}
//	public Integer getStatC() {
//		return statC;
//	}
//	public void setStatC(Integer statC) {
//		this.statC = statC;
//	}
//	public Integer getStatD() {
//		return statD;
//	}
//	public void setStatD(Integer statD) {
//		this.statD = statD;
//	}
//
//	public void setStats(UserAnswerStat userAnswerStat) {
//
//		this.statA = userAnswerStat.getOptionA();
//		this.statB = userAnswerStat.getOptionB();
//		if(this.optionC!=null)
//			this.statC = userAnswerStat.getOptionC();
//		if(this.optionD!=null)
//			this.statD = userAnswerStat.getOptionD();
//		
//		this.totalUsers = this.statA+this.statB+this.statC+this.statD;
//	}
//
//	public Integer getTotalUsers() {
//		return totalUsers;
//	}
//
//	public void setTotalUsers(Integer totalUsers) {
//		this.totalUsers = totalUsers;
//	}	

}
