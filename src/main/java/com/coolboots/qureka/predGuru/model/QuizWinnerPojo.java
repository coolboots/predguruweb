package com.coolboots.qureka.predGuru.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class QuizWinnerPojo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8289120245001635775L;
	private Long quizId;
	private String userId;
	private double money;
	private long coin;
	
	private UserPojo userPojo;
	
	public Long getQuizId() {
		return quizId;
	}
	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public long getCoin() {
		return coin;
	}
	public void setCoin(long coin) {
		this.coin = coin;
	}
	@JsonProperty("user")
	public UserPojo getUserPojo() {
		return userPojo;
	}
	public void setUserPojo(UserPojo userPojo) {
		this.userPojo = userPojo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof QuizWinnerPojo))
			return false;
		QuizWinnerPojo other = (QuizWinnerPojo) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
	
	
	
	
}
