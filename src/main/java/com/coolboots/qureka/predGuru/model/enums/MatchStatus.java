package com.coolboots.qureka.predGuru.model.enums;

public enum MatchStatus {

	NOT_COMPLETED, COMPLETED, IN_PROCESS;
	
}
