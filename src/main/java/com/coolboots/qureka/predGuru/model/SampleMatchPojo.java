package com.coolboots.qureka.predGuru.model;

import java.util.List;

public class SampleMatchPojo {

	private Long matchId;
	private String teamA;
	private String teamAUrl;	
	private String teamB;
	private String teamBUrl;
	
	private List<SampleQuestionPojo> questions;
	public Long getMatchId() {
		return matchId;
	}
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	public String getTeamA() {
		return teamA;
	}
	public void setTeamA(String teamA) {
		this.teamA = teamA;
	}
	public String getTeamAUrl() {
		return teamAUrl;
	}
	public void setTeamAUrl(String teamAUrl) {
		this.teamAUrl = teamAUrl;
	}
	public String getTeamB() {
		return teamB;
	}
	public void setTeamB(String teamB) {
		this.teamB = teamB;
	}
	public List<SampleQuestionPojo> getQuestions() {
		return questions;
	}
	public void setQuestions(List<SampleQuestionPojo> questions) {
		this.questions = questions;
	}
	public String getTeamBUrl() {
		return teamBUrl;
	}
	public void setTeamBUrl(String teamBUrl) {
		this.teamBUrl = teamBUrl;
	}
	
	
}
