package com.coolboots.qureka.predGuru.model;

import java.util.Date;

public class UserMatch {
	
	private Long id;
	private String userId;
	private Long matchId;
	private boolean paid;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getMatchId() {
		return matchId;
	}
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	
	@Override
	public String toString() {
		return "UserMatch [id=" + id + ", userId=" + userId + ", matchId=" + matchId + ", paid=" + paid + "]";
	}
	
	
	
}
