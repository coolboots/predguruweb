package com.coolboots.qureka.predGuru.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserAnswerStat {
	
	private Long questionId;
	private Integer optionA;
	private Integer optionB;
	private Integer optionC;
	private Integer optionD;
	
	@JsonProperty("question_id")
	public Long getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}
	public Integer getOptionA() {
		return optionA;
	}
	public void setOptionA(Integer optionA) {
		this.optionA = optionA;
	}
	public Integer getOptionB() {
		return optionB;
	}
	public void setOptionB(Integer optionB) {
		this.optionB = optionB;
	}
	public Integer getOptionC() {
		return optionC;
	}
	public void setOptionC(Integer optionC) {
		this.optionC = optionC;
	}
	public Integer getOptionD() {
		return optionD;
	}
	public void setOptionD(Integer optionD) {
		this.optionD = optionD;
	}
	
	@Override
	public String toString() {
		return "UserAnswerStat [questionId=" + questionId + ", optionA=" + optionA + ", optionB=" + optionB
				+ ", optionC=" + optionC + ", optionD=" + optionD + "]";
	}
	
	
}
