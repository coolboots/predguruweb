package com.coolboots.qureka.predGuru.model; 

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UserPojo implements Serializable{
	
	private static final long serialVersionUID = 3486246729455811139L;
	private String id;
	private String firstName;
	private String displayName;
	private String image;
	private String city;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		if(firstName!=null) {
			this.firstName = firstName;
		}else {
			this.firstName ="";	
		}
	}
	
	@JsonProperty("display_user_name")
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@JsonProperty("profile_image")
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		if(image!=null) {
			this.image = image;
		}else {
			this.image = "";
		}
		
	}
	
	@JsonProperty("city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Override
	public String toString() {
		return "UserPojo [id=" + id + ", firstName=" + firstName + ", displayName=" + displayName + ", image=" + image
				+ ", city=" + city + "]";
	}
}
