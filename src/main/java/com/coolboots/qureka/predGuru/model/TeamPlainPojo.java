package com.coolboots.qureka.predGuru.model;


import java.util.Date;

public class TeamPlainPojo {

    private Long id;
    private String name;
    private String shortName;
    private Date addedOn;
    private Date updatedOn;
    private Boolean isActive;
    private String imageUrl;
    private String flagUrl;
    private String logoUrl;

   

    public TeamPlainPojo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Boolean getIsActive() {
        return isActive;
    }


    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

	@Override
	public String toString() {
		return "TeamPlainPojo [id=" + id + ", name=" + name + ", shortName=" + shortName + ", addedOn=" + addedOn
				+ ", updatedOn=" + updatedOn + ", isActive=" + isActive + ", imageUrl=" + imageUrl + ", flagUrl="
				+ flagUrl + ", logoUrl=" + logoUrl + "]";
	}
    
    
}
