package com.coolboots.qureka.predGuru.model;

import java.util.Date;

import com.coolboots.qureka.predGuru.model.enums.MatchStatus;



public class MatchComplete {

	
	private Long matchId;
	private Date date;
	private MatchStatus matchStatus;
	
	public Long getMatchId() {
		return matchId;
	}
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public MatchStatus getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(MatchStatus matchStatus) {
		this.matchStatus = matchStatus;
	}
	
	
}
