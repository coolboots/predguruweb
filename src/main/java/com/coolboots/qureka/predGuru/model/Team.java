package com.coolboots.qureka.predGuru.model;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Team {	
	
	private Long id;
	private String name;
	private String shortName;
	private Date addedOn;
	private Date updatedOn;
	private String imageUrl;
	private String flagUrl;
	private String logoUrl;
	
	private MultipartFile image;
	private MultipartFile flag;
	private MultipartFile logo;
	
	private Boolean isActive;
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getShortName() {
		return shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public Date getAddedOn() {
		return addedOn;
	}


	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}


	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}


	public String getFlagUrl() {
		return flagUrl;
	}

	public void setFlagUrl(String flagUrl) {
		this.flagUrl = flagUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}


	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	
	public MultipartFile getImage() {
		return image;
	}


	public void setImage(MultipartFile image) {
		this.image = image;
	}

	
	public MultipartFile getFlag() {
		return flag;
	}


	public void setFlag(MultipartFile flag) {
		this.flag = flag;
	}

	
	public MultipartFile getLogo() {
		return logo;
	}


	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}



	
	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	/*@JsonProperty("imageUrl")
	public String returnFullImageUrl() {
		
		if(imageUrl!=null && !imageUrl.equals("")){
			if(imageUrl.contains(Constants.TEAM_IMAGE_URL)) return imageUrl;
			return Constants.TEAM_IMAGE_URL+imageUrl;
		}
		return null;
	}*/
	
	/*@JsonProperty("flagUrl")
	public String returnFullFlagUrl() {

		if(flagUrl!=null && !flagUrl.equals("")){
			if(flagUrl.contains(Constants.TEAM_IMAGE_URL)) return flagUrl;
			return Constants.TEAM_IMAGE_URL+flagUrl;
		}
		return null;
	}*/
	
	/*@JsonProperty("logoUrl")
	public String returnFullLogoUrl() {
	
		if(logoUrl!=null && !logoUrl.equals("")){
			if(logoUrl.contains(Constants.TEAM_IMAGE_URL)) return logoUrl;
			return Constants.TEAM_IMAGE_URL+logoUrl;
		}
		return null;
	}
	*/
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if(!(o instanceof Team)) {
			return false;
		}
			
		final Team team = (Team) o;
		
		return !(name != null ? !name.equals(team.getName()) : team.getName() != null);
		
	}


	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", shortName=" + shortName + ", addedOn=" + addedOn
				+ ", updatedOn=" + updatedOn + ", imageUrl=" + imageUrl + ", flagUrl=" + flagUrl + ", logoUrl="
				+ logoUrl + ", isActive=" + isActive + "]";
	}


	@Override
	public int hashCode() {
		return (name != null ? name.hashCode() : 0);
	}
	
	/*@Override
	public String returnJsonKey() {
		return ""+id;
	}*/
	
}
