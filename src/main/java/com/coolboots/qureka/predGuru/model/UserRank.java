package com.coolboots.qureka.predGuru.model; 

import java.io.Serializable;

import com.coolboots.qureka.predGuru.user.model.AppUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRank implements Serializable{


	private static final long serialVersionUID = 2150485562597975243L;
	

	private Long matchId;
	private String userId;
	private Long time;
	private Long score;
	private Long rank;
	private Long money;
	private Long correctAnswers;
	private Long wrongAnswers;
	private Long totalAnswers;
	private Long totalQuestions;
	private Long streaks;
	private UserPojo userPojo;
	private AppUser appUser;
	private int coins;
	private String name;
	private String image;
	
	
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMatchId() {
		return matchId;
	}
	
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	
	@JsonIgnore
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	
	
	public Long getScore() {
		return score;
	}
	public void setScore(Long score) {
		this.score = score;
	}
	

	public Long getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(Long correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	
	public Long getWrongAnswers() {
		return wrongAnswers;
	}

	public void setWrongAnswers(Long wrongAnswers) {
		this.wrongAnswers = wrongAnswers;
	}

	
	public Long getTotalAnswers() {
		return totalAnswers;
	}

	public void setTotalAnswers(Long totalAnswers) {
		this.totalAnswers = totalAnswers;
	}

	
	public Long getRank() {
		return rank;
	}
	public void setRank(Long rank) {
		this.rank = rank;
	}
	
	
	public Long getMoney() {
		return money;
	}
	public void setMoney(Long money) {
		this.money = money;
	}
	
	public Long getTotalQuestions() {
		return totalQuestions;
	}

	public void setTotalQuestions(Long totalQuestions) {
		this.totalQuestions = totalQuestions;
	}

	public Long getStreaks() {
		return streaks;
	}

	public void setStreaks(Long streaks) {
		this.streaks = streaks;
	}
	

	@JsonProperty("user")
	public UserPojo getUserPojo() {
		return userPojo;
	}
	
	@JsonIgnore
	public UserPojo returnUserPojo() {	
		return userPojo;
	}
	

	public void setUserPojo(UserPojo userPojo) {
		this.userPojo = userPojo;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	@JsonIgnore
	public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appUser == null) ? 0 : appUser.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "UserRank [matchId=" + matchId + ", userId=" + userId + ", time=" + time + ", score=" + score + ", rank="
				+ rank + ", money=" + money + ", correctAnswers=" + correctAnswers + ", wrongAnswers=" + wrongAnswers
				+ ", totalAnswers=" + totalAnswers + ", totalQuestions=" + totalQuestions + ", streaks=" + streaks
				+ ", userPojo=" + userPojo + ", appUser=" + appUser + ", coins=" + coins + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRank other = (UserRank) obj;
		if (appUser == null) {
			if (other.appUser != null)
				return false;
		} else if (!appUser.equals(other.appUser))
			return false;
		return true;
	}

}
