package com.coolboots.qureka.predGuru.model;

public class Data {

	private String data;
	private int score;
	private String answer;
	private String quizId;
	private Long questionId;
	private String userId;
	private String clickOption;
	private String subDomain;
	
	
	
	
	public String getSubDomain() {
		return subDomain;
	}

	public void setSubDomain(String subDomain) {
		this.subDomain = subDomain;
	}

	public String getClickOption() {
		return clickOption;
	}

	public void setClickOption(String clickOption) {
		this.clickOption = clickOption;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	@Override
	public String toString() {
		return "Data [data=" + data + ", score=" + score + ", answer=" + answer + ", quizId=" + quizId + ", questionId="
				+ questionId + ", userId=" + userId + "]";
	}

	

	

	
	
	
}
