package com.coolboots.qureka.predGuru.model;

import java.util.Date;

public class PrizeMatrix {

	private Long matchId;
	private Long moneyPool=0l;
	private Long coins=0l;
	private Boolean isActive;
	private String rank;
	
	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Long getMoneyPool() {
		return moneyPool;
	}

	public void setMoneyPool(Long moneyPool) {
		this.moneyPool = moneyPool;
	}

	public Long getCoins() {
		return coins;
	}

	public void setCoins(Long coins) {
		this.coins = coins;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "PrizeMatrix [matchId=" + matchId + ", moneyPool=" + moneyPool + ", coins=" + coins + ", isActive="
				+ isActive + ", rank=" + rank + "]";
	}

	
	
	
}
