package com.coolboots.qureka.predGuru.model; 


import java.util.Date;

import com.coolboots.qureka.predGuru.model.enums.Answer;

public class UserAnswer {

	private Long id;
	private Answer givenOption;
	private int streak;
	private int score;
	private Long question;
	private String appUser;
	
	private Long matchId;
	private Date createdOn;


	public Long getId() {
		return id;
	}

	public Answer getGivenOption() {
		return givenOption;
	}

	public int getStreak() {
		return streak;
	}

	public int getScore() {
		return score;
	}
	
	
	public Long getQuestion() {
		return question;
	}

	
	public String getAppUser() {
		return appUser;
	}
	
	public Long getMatchId() {
		return matchId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setGivenOption(Answer givenOption) {
		this.givenOption = givenOption;
	}

	public void setStreak(int streak) {
		this.streak = streak;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setQuestion(Long question) {
		this.question = question;
	}
	
	public void setAppUser(String appUser) {
		this.appUser = appUser;
	}
	
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	 
	public void setAppUserId(String appuserId) {
		this.appUser=appuserId;
	}

	public void setQuestionId(Long questionId) {
		this.question = questionId;
	}
	
	@Override
	public String toString() {
		return "UserAnswer{" +
				"givenOption=" + givenOption +
				", question=" + question +
				", appUser=" + appUser +
				", matchId=" + matchId +
				'}';
	}
}
