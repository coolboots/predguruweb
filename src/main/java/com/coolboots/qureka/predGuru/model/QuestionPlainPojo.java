package com.coolboots.qureka.predGuru.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class QuestionPlainPojo {

    private Long id;
    private String questionSet;
    private Long matchId;
    private String question;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String funFact;
    private Long mark;
    private Integer sequence;

    private Date startOn;
    private Boolean live;
    private Date liveOn;
    private Boolean isActive;


    public QuestionPlainPojo() {
    }
    
    public QuestionPlainPojo(Question question) {
        this.id = question.getId();
        this.questionSet = question.getQuestionSet().getId().toString();

        this.matchId = question.getMatchId();
        this.question = question.getQuestion();
        this.optionA = question.getOptionA();
        this.optionB = question.getOptionB();
        this.optionC = question.getOptionC();
        this.optionD = question.getOptionD();
        this.funFact = question.getFunFact();
        this.mark = question.getMark();
        this.sequence = question.getSequence();
        this.startOn = question.getStartOn();
        this.live = question.getLive();
        this.liveOn = question.getLiveOn();
        this.isActive = question.getIsActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionSet() {
        return questionSet;
    }

    public void setQuestionSet(String questionSet) {
        this.questionSet = questionSet;
    }

    @JsonProperty("match_id")
    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @JsonProperty("option_a")
    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    @JsonProperty("option_b")
    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    @JsonProperty("option_c")
    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    @JsonProperty("option_d")
    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getFunFact() {
        return funFact;
    }

    public void setFunFact(String funFact) {
        this.funFact = funFact;
    }

    public Long getMark() {
        return mark;
    }

    public void setMark(Long mark) {
        this.mark = mark;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }


    @JsonProperty("start_on")
    public Date getStartOn() {
        return startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }


    public Boolean getLive() {
        return live;
    }

    public void setLive(Boolean live) {
        this.live = live;
    }

    @JsonProperty("live_on")
    public Date getLiveOn() {
        return liveOn;
    }

    public void setLiveOn(Date liveOn) {
        this.liveOn = liveOn;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

	@Override
	public String toString() {
		return "QuestionPlainPojo [id=" + id + ", questionSet=" + questionSet + ", matchId=" + matchId + ", question="
				+ question + ", optionA=" + optionA + ", optionB=" + optionB + ", optionC=" + optionC + ", optionD="
				+ optionD + ", funFact=" + funFact + ", mark=" + mark + ", sequence=" + sequence + ", startOn="
				+ startOn + ", live=" + live + ", liveOn=" + liveOn + ", isActive=" + isActive + "]";
	}
    
    
}
