package com.coolboots.qureka.predGuru;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisTemplateService {

	@Autowired
	public RedisTemplate<String, Object> redisTemplate;


	public RedisTemplate<String, Object> getRedisTemplate(){
		return redisTemplate;
	}


	public String readFromRedis(String key) {
		String string="";
		try {

			Object valueObject = redisTemplate.opsForValue().get(key);
			string = (valueObject!=null)? valueObject.toString():"";
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
		}
		return string;
	}

	public Boolean writeToRedis(String key, String s, Long timeout, TimeUnit unit) {
		try {
			if(timeout==null || timeout==-1l) {
				redisTemplate.opsForValue().set(key, s);
			}else {
				redisTemplate.opsForValue().set(key, s, timeout, unit);
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public void increment(String key) {
		try {
			if(redisTemplate.opsForValue().setIfAbsent(key,1)) {
				redisTemplate.expire(key, 25L, TimeUnit.MINUTES);
			}else {
				redisTemplate.opsForValue().increment(key, 1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
		}
	}

	public void decrement(String key) {
		try {
			//			if(redisTemplate.opsForValue().setIfAbsent(key,1)) {
			//				redisTemplate.expire(key, 25L, TimeUnit.MINUTES);
			//			}else {
			redisTemplate.opsForValue().increment(key, -1);
			//			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
		}
	}

	public void closeConnection(RedisTemplate<String, Object> redisTemplate){
		try {

			LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory) redisTemplate.getConnectionFactory();
			connectionFactory.getConnection().close();
		}catch (RedisConnectionFailureException e){
			e.printStackTrace();
		}
	}

	public void hashIncrement(String key,String field, Long timeout,TimeUnit unit) {
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			if(hashOperations.putIfAbsent(key, field, "1")) {
				redisTemplate.expire(key, timeout, unit);
			}else {
				hashOperations.increment(key, field, 1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	public Set<String> readHashFields(String key) {

		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		return hashOperations.keys(key);

	}


	public void readHashValuesFromFields(String key, Set<String> fields) {

		HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
		hashOperations.multiGet(key, fields);

	}


	public String readHashValueFromField(String key, String field) {
		String s = "";
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			Object valueObject = hashOperations.get(key, field);
			s = (valueObject!=null)? valueObject.toString():"";
		}catch (Exception e) {
			e.printStackTrace();
		}
		return s;

	}


	public String readAllHashValues(String hashKey) {

		String s = "";

		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			List<String> objects = hashOperations.values(hashKey);

			s = (objects!=null && objects.size()>0)?objects.toString():"";
		}catch (Exception e) {
			e.printStackTrace();
		}

		return s;
	}

	public Boolean writeHashValueToField(String key,String field,String value) {

		try {
			HashOperations<String , String, String> hashOperations = redisTemplate.opsForHash();
			hashOperations.put(key, field, value);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public void hashIncrement(String key,String field) {
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			hashOperations.increment(key, field, 1);

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Long hashIncrementReturn(String key,String field) {
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			return hashOperations.increment(key, field, 1);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0l;
		
	}
	public boolean deleteValueFromHash(String hashKey, String field) {
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			hashOperations.delete(hashKey, field);
			
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public Map<String, String> readHashHgetAll(String hashKey) {

		Map<String, String> hgetAll = null;
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			Map<String, String> objects = hashOperations.entries(hashKey);

			if(objects!=null && objects.size()>0) {
				hgetAll = objects;
			}

		}catch (Exception e) {
			e.printStackTrace();
		}

		return hgetAll;
	}


	public Boolean writeHashObjectToField(String key,String field,Object value) {

		try {
			HashOperations<String , String, Object> hashOperations = redisTemplate.opsForHash();
			hashOperations.put(key, field, value);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public Boolean deleteFieldFromHash(String key, String field) {
		try {
			HashOperations<String , String, Object> hashOperations = redisTemplate.opsForHash();
			hashOperations.delete(key, field);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public int getSizeOfHashValues(String hashKey) {

		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			//			Map<String, String> objects = hashOperations.entries(hashKey);
			//			if(objects!=null && objects.size()>0) return objects.size();

			List<String> objects = hashOperations.values(hashKey);
			if(objects!=null && objects.size()>0) return objects.size();

		}catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}


	public List<Object> readListFromRedis(String key,Long start,Long end){
		try {
			ListOperations<String, Object> listOperations = redisTemplate.opsForList();
			return listOperations.range(key, start, end);
		}catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public Object rPush(String key,String value) {

		try {
			ListOperations<String, Object> listOperations = redisTemplate.opsForList();
			if(listOperations!=null) {
				listOperations.rightPush(key, value);

				Long length = listOperations.size(key);

				if(length!=null && length>20)  return listOperations.leftPop(key);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	public long getLengthOfList(String key) {
		try {
			ListOperations<String, Object> listOperations = redisTemplate.opsForList();
			if(listOperations!=null) {
				return listOperations.size(key);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 0;
	}


	public boolean deleteKeys(List<String> keys) {

		try {
			redisTemplate.delete(keys);
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public boolean deleteKey(String keys) {

		try {
			redisTemplate.delete(keys);
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public Boolean writeList(String key, String values) {

		try {
			//redisTemplate.opsForList().rightPushAll(key, values);
			
			 redisTemplate.opsForList().leftPush(key, values);
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<Object> readListRange(String key) {
		
		List<Object> list = new ArrayList<>();
		try {
			//redisTemplate.opsForList().rightPushAll(key, values);
			Long keySize	=	redisTemplate.opsForList().size(key);
			

			list  =	redisTemplate.opsForList().range(key, 0, keySize); 
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}	
	
	public void hashDecrement(String key,String field) {
		try {
			HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
			hashOperations.increment(key, field, -1);

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
