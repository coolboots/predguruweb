package com.coolboots.qureka.predGuru;

public class Constants {

//	public static String API_URL                		=   "http://103.94.108.146:8081/qureka/";

//	public static String API_URL                		=   "http://192.168.0.46:8081/qureka/";

	public static String API_URL = "http://api2.qureka.com/";
	public static String SIGNUP_API_URL = "http://api2.qureka.com/";

//	public static String API_URL                		=   "http://localhost:9999/";
//	public static String SIGNUP_API_URL                 =   "http://103.94.108.146:8081/qureka";
//	public static String SIGNUP_API_URL                		=   "http://192.168.0.46:8081/qureka/";
//	public static String SIGNUP_API_URL                		=   "http://localhost:9999/";

	public static int DAILY_COIN_BONUS = 50;
	public static int WELCOME_COIN_BONUS = 100;
	public static long WATCH_VIDEO_COIN = 10;
	public static long INTRO_COIN_BONUS = 100;

	/* reason for PREDGURU_WEB */
	public static String PRED_DAILY_BONUS = "P_DAILY_BONUS";
	public static String PRED_WELCOME_BONUS = "P_WELCOME_BONUS";
	public static String PRED_INTRO_QUESTIONS_PLAY_BONUS = "P_INTRO_QUESTION_BONUS";

	public static double round(double value, int places) {

		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
}
