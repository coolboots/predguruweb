package com.coolboots.qureka.predGuru.wallet.service;

import com.coolboots.qureka.predGuru.handler.Response;

public interface WalletService {
	
	Response coinWalletDetails(String userId);
	
	Response walletDetails(String userId);
	
	Response redeemAmount(String userId,String amount);

}
