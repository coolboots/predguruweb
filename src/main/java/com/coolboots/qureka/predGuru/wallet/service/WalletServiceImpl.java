package com.coolboots.qureka.predGuru.wallet.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.model.CoinWalletDetails;
import com.coolboots.qureka.predGuru.user.model.WalletDetails;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WalletServiceImpl implements WalletService{

	@Autowired com.coolboots.qureka.predGuru.RedisTemplateService redisTemplateService;


	@Override
	public com.coolboots.qureka.predGuru.handler.Response coinWalletDetails(String userId) {

		Response resObj = new Response();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v1/user/coinWalletDetails";
			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("userId", userId)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {
					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					ObjectMapper objectMapper = new ObjectMapper();
					List<CoinWalletDetails> coinWalletDetails = objectMapper.readValue(obj, new TypeReference<ArrayList<CoinWalletDetails>>() {});
					
				    resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					resObj.setObject(coinWalletDetails );
					resObj.setMessage("success");

				}else {
					resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					resObj.setObject("");
					resObj.setMessage("fail");
				}
			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}
		
		return resObj;

	}
	
	@Override
	public Response walletDetails(String userId) {
	
		Response resObj = new Response();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v1/user/walletDetails";


			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("userId", userId)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					ObjectMapper objectMapper = new ObjectMapper();
					List<WalletDetails> walletDetailsList  = objectMapper.readValue(obj, new TypeReference<ArrayList<WalletDetails>>() {});
					
				    resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					resObj.setObject(walletDetailsList );
					resObj.setMessage("success");
				}else {
					resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					resObj.setObject("");
					resObj.setMessage("fail");
					}
			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}
		return resObj;

	}
	
	@Override
	public Response redeemAmount(String userId, String amount) {
	
		Response resObj = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v4/user/redeemAmount";

			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("userId", userId)
					.addParameter("amount", amount)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);

			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));


					redisTemplateService.writeToRedis("reedemAmount", obj, 30l, TimeUnit.MINUTES);
					
					resObj.setMessage("success");
			        resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
			        resObj.setObject("");
			        return resObj;
			        
					
				}else {
					resObj.setMessage("success");
			        resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
			        resObj.setObject("");
			        return resObj;
			        
				}
			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return resObj;

	}
}
