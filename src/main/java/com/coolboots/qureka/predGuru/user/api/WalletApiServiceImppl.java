package com.coolboots.qureka.predGuru.user.api;

import java.net.URI;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class WalletApiServiceImppl implements WalletApiService{

	@Override
	public Response wallet(String userId) {
		
		Response response = new Response();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			
			String url = Constants.API_URL+"api/v1/user/wallets";
			
			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("userId", userId)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {
									
					String walletStr = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					
					ObjectMapper objectMapper = new ObjectMapper();
					WalletDto walletDto = objectMapper.readValue(walletStr, WalletDto.class);
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject(walletDto);
					
			        return response;
			    }else {
			    	response.setMessage(String.valueOf(EntityUtils.toString(httpResponse.getEntity())));
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject("");
			        return response;
					
			    }
				
			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}

		return response; 
	}

}
