package com.coolboots.qureka.predGuru.user.api;

import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.model.AppUser;

public interface AppUserApiService {

	Response signUpApiCall(String firstName, String mobile, String url, String string);

	Response verifyApiCall(String mobile, String otp);

	Response minusCoinApiCall(String userId, Long quizId, int entryFees, String type, String apppName);

	// sResponse addCoinsApiCall(String id, long introCoinBonus, String appName);

	Response addCoinsApiCall(String userId, long coin, String appName, String res);

	Response updateToken(AppUser user);

}
