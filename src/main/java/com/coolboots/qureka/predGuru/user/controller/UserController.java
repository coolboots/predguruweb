package com.coolboots.qureka.predGuru.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.dto.UserRegistrationDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.api.AppUserApiService;
import com.coolboots.qureka.predGuru.user.model.AppUser;
import com.coolboots.qureka.predGuru.user.service.UserService;

@Controller
@RequestMapping("")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;

	@Autowired
	AppUserApiService appUserApiService;

	@Autowired
	RedisTemplateService redisTemplateService;

	@ModelAttribute("user")
	public UserRegistrationDto userRegistrationDto() {
		return new UserRegistrationDto();
	}

	@GetMapping("/signup")
	public String showSignupForm(Model model, HttpServletRequest request) {

		if (CookieConfig.getCookies(request, "userId") != null) {
			return "redirect:/";
		}

		model.addAttribute("logout_m", request.getSession().getAttribute("logout_m"));
		model.addAttribute("introBonus", Constants.INTRO_COIN_BONUS);
		request.getSession().removeAttribute("logout_m");
		return "signup/signup";
	}

	@PostMapping("/signup")
	public @ResponseBody Response registerUserAccount(@RequestBody UserRegistrationDto userDto, BindingResult result,
			HttpServletRequest request, HttpServletResponse responseS) {
		System.out.println(userDto.getFirstName() + userDto.getInfo3());

		String url = userService.getUrl(userDto.getInfo3());

		CookieConfig.connect(responseS, "getSubDomain", url);

		Response response = appUserApiService.signUpApiCall(userDto.getFirstName(), userDto.getEmail(), url,
				userDto.getInfo1());

		String fName = userDto.getFirstName();
		String[] fiName = fName.split(" ");
		String appName = CookieConfig.getCookies(request, "getSubDomain");
		System.out.println(response.getStatus());
		try {
			if (response.getObject() != null && response.getStatus() == 200) {

				AppUser appUser = (AppUser) response.getObject();
				System.out.println(appUser);

				// request.getSession().setAttribute("current user added date",
				// appUser.getAddedOn());

				CookieConfig.connect(responseS, "userId", appUser.getId());

				String coinsSession = (String) request.getSession().getAttribute("addcoins");
				System.out.println("appName : " + appName);
				System.out.println("coinsSession " + coinsSession);
				if (coinsSession != null) {
					appUserApiService.addCoinsApiCall(appUser.getId(), Constants.INTRO_COIN_BONUS, appName,
							Constants.PRED_INTRO_QUESTIONS_PLAY_BONUS);
				}

				if (appUser.getUserTag().equals("NEW_USER")) {
					Response addCoinResp = appUserApiService.addCoinsApiCall(appUser.getId(),
							Constants.WELCOME_COIN_BONUS, appName, Constants.PRED_WELCOME_BONUS);
					if (addCoinResp.getStatus() == 200) {
						request.getSession().setAttribute("userTag", appUser.getUserTag());
					}
				}

				userService.userWallet(appUser.getId());
				CookieConfig.connect(responseS, "userId", appUser.getId());
				CookieConfig.connect(responseS, "profileImage", appUser.getProfileImage());
				CookieConfig.connect(responseS, "loginType", userDto.getInfo1());
				request.getSession().setAttribute("userId", appUser.getId());
				request.getSession().setAttribute("profileImage", appUser.getProfileImage());
			} else {
				request.getSession().setAttribute("message", response.getMessage());
			}
		} catch (Exception e) {
			System.out.println("Exception occured inside the signup method in the MainController: " + e.getMessage());
		}
		return response;
	}

//
//	@GetMapping("/otp")
//	public String login(Model model, HttpServletRequest request) {
//
//		model.addAttribute("mobile", CookieConfig.getCookies(request, "mobile"));
//
//		if (CookieConfig.getCookies(request, "mobile") == null) {
//			return "redirect:/signup";
//		}
//
//		if (CookieConfig.getCookies(request, "userId") != null) {
//			return "redirect:/";
//		}
//
//		model.addAttribute("message", request.getSession().getAttribute("message"));
//		request.getSession().removeAttribute("message");
//
//		return "signup/otp";
//	}
//
//	
//
//	@GetMapping("/resend")
//	public String resendOtp(HttpServletRequest request) {
//
//		if (CookieConfig.getCookies(request, "userId") != null) {
//			return "redirect:/";
//		}
//
//		if (CookieConfig.getCookies(request, "mobile") != null
//				|| CookieConfig.getCookies(request, "firstName") != null) {
//
//			String mobile = CookieConfig.getCookies(request, "mobile");
//			String firstName = CookieConfig.getCookies(request, "firstName");
//			String url = CookieConfig.getCookies(request, "getSubDomain");
//
//			Response response = appUserApiService.signUpApiCall(firstName, mobile, url);
//			if (response.getStatus() == 200) {
//				request.getSession().setAttribute("message", response.getMessage());
//				if (response.getMessage().equals("OTP Sent")) {
//					return "redirect:/otp?success";
//				} else {
//					return "redirect:/otp?error";
//				}
//			}
//		}
//		return "/signup/resend";
//	}
//

	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {

		request.getSession().invalidate();
		CookieConfig.remove(request, response);

		request.getSession().setAttribute("logout_m", "You have successfully logged out");

		/* return "redirect:/intro"; */
		return "index";
	}

	@ResponseBody
	@GetMapping("/user-rest/update-fcm-token")
	public String updateToken(String token, HttpServletRequest request) {

//		if (CookieConfig.getCookies(request, "email") != null
//				|| CookieConfig.getCookies(request, "firstName") != null) {

			String userId = CookieConfig.getCookies(request, "userId");
			
			if(userId != null) {
		//	String email = CookieConfig.getCookies(request, "email");
			String appName =  CookieConfig.getCookies(request, "getSubDomain");

			AppUser user = new AppUser();
			user.setId(userId);
//			user.setEmail(email);
			user.setFcmid(token);
			user.setInfo3(appName);

			Response response = appUserApiService.updateToken(user);
			if (response.getStatus() == 200) {
				return "sucess";
			} else {
				return "failed";
			}
		}

		return "failed";
	}

}
