package com.coolboots.qureka.predGuru.user.service;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.api.AppUserApiService;
import com.coolboots.qureka.predGuru.user.api.WalletApiService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private RedisTemplateService redisTemplateService;

	@Autowired
	WalletApiService walletApiService;

	@Autowired
	AppUserApiService appUserApiService;

	@Override
	public void userWallet(String userId) {

		try {
			Response walletResponse = walletApiService.wallet(userId);
			setUserWalletInRedis(walletResponse, userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setUserWalletInRedis(Response walletResponse, String userId) {

		if (walletResponse.getObject() != null) {

			WalletDto walletDto = (WalletDto) walletResponse.getObject();
			String hashKey = "userWallet";
			redisTemplateService.deleteValueFromHash(hashKey, "" + userId);

			ObjectMapper mapper = new ObjectMapper();
			String walletDtoStr = null;
			try {
				walletDtoStr = mapper.writeValueAsString(walletDto);
				redisTemplateService.writeHashObjectToField(hashKey, userId, walletDtoStr);

			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public WalletDto getWalletAmount(String userId, String userWall) {

		WalletDto walletDto = null;
		try {
			if (!userWall.equals("")) {
				ObjectMapper objectMapper = new ObjectMapper();
				walletDto = objectMapper.readValue(userWall, WalletDto.class);
				if (walletDto != null) {
					return walletDto;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return walletDto;

	}

	@Override
	public String userDailyBonus(String userId, int coin, HttpServletResponse htResponse, HttpServletRequest request) {

		LocalDate date = LocalDate.now();
		String fields = userId + "_" + date;

		LocalDate oldDate = date.minusDays(1);
		String oldFields = userId + "" + oldDate;

		if (coin >= 100) {
			return null;
		}
		/*
		 * if(!fields.equals(oldFields)) { CookieConfig.connect(htResponse, fields,
		 * null); }
		 */

		// if(CookieConfig.getCookies(request, fields) == null) {

		// String dailyBonusKey = "daily_50_coin_bonus";
		String dailyBonusKey = Constants.PRED_DAILY_BONUS;
		String dailyBonusStr = redisTemplateService.readHashValueFromField(dailyBonusKey, fields);
		String appName = CookieConfig.getCookies(request, "getSubDomain");

		if (dailyBonusStr.equals("")) {

			Response addCoinResp = appUserApiService.addCoinsApiCall(userId, Constants.DAILY_COIN_BONUS, appName,
					Constants.PRED_DAILY_BONUS);

			if (addCoinResp.getStatus() == 200) {
				redisTemplateService.deleteFieldFromHash(dailyBonusKey, oldFields);

				redisTemplateService.writeHashObjectToField(dailyBonusKey, fields, "Yes");
				CookieConfig.connect(htResponse, fields, "yes");

				return "yes";
			}
		}
		// }
		return null;
	}

	@Override
	public String getCurrentUrl(HttpServletRequest request) {

		URL url;
		try {
			url = new URL(request.getRequestURL().toString());

			String host = url.getHost();
			String userInfo = url.getUserInfo();
			String scheme = url.getProtocol();
			int port = url.getPort();
			String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
			String query = (String) request.getAttribute("javax.servlet.forward.query_string");
			URI uri;
			uri = new URI(scheme, userInfo, host, port, path, query, null);
			String dataArr[] = uri.toString().split("http://");
			String dataArrs[] = null;
			if (dataArr[1] != null) {
				dataArrs = dataArr[1].split(".com");
			}

			return dataArrs[0];

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getUrl(String url) {

		if (url.equals("localhost")) {
			System.out.println("url -" + url);
			return url;
		} else if (url.equals("103.94.108.147")) {
			return "147-server";
		} else if (url.equals("play.qureka.com")) {
			return "play.qureka";
		} else {
			String dataArr[] = url.split(".com");
			return dataArr[0];
		}

	}

	@Override
	public Long findDifference(Date quizDate) {
		try {

			Date d1 = new Date();
			Date d2 = quizDate;

			long duration = d2.getTime() - d1.getTime();

			return duration;

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return 0l;

	}

}
