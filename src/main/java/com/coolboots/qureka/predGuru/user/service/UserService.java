package com.coolboots.qureka.predGuru.user.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coolboots.qureka.predGuru.dto.WalletDto;
import com.coolboots.qureka.predGuru.handler.Response;

public interface UserService {

	void userWallet(String string);

	void setUserWalletInRedis(Response walletResponse, String userId);

	WalletDto getWalletAmount(String userId, String userWall);

	String userDailyBonus(String userId, int coinBalance, HttpServletResponse htResponse, HttpServletRequest request);

	String getCurrentUrl(HttpServletRequest request);

	String getUrl(String info3);

	Long findDifference(Date quizDate);

}
