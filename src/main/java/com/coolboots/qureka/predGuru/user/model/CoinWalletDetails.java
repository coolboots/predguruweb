package com.coolboots.qureka.predGuru.user.model;

import java.util.Date;


public class CoinWalletDetails  {


	private Long id;

	private Long quizId;

	private Long coinWallet;

	private String mode;

	private String activityDetails;

	private Date date;

	private Integer coins;

	private String reason;

	private String appName;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}


	public Long getCoinWallet() {
		return coinWallet;
	}

	public void setCoinWallet(Long coinWallet) {
		this.coinWallet = coinWallet;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getActivityDetails() {
		return activityDetails;
	}

	public void setActivityDetails(String activityDetails) {
		this.activityDetails = activityDetails;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCoins() {
		return coins;
	}

	public void setCoins(Integer coins) {
		this.coins = coins;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String toString() {
		return "CoinWalletDetail [id=" + id + ", quizId=" + quizId + ", mode=" + mode
				+ ", activityDetails=" + activityDetails + ", date=" + date + ", coins=" + coins + ", reason=" + reason
				+ "]";
	}




}
