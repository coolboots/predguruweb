package com.coolboots.qureka.predGuru.user.api;

import java.net.URI;
import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.user.model.AppUser;
import com.coolboots.qureka.predGuru.user.service.UserService;
import com.coolboots.qureka.predGuru.utils.AESCrypto;
import com.coolboots.qureka.predGuru.utils.DynamicApiKeyConstant;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AppUserApiServiceImpl implements AppUserApiService {
	private static final Logger logger = LoggerFactory.getLogger(AppUserApiServiceImpl.class);

	@Autowired
	WalletApiService walletApiService;

	@Autowired
	RedisTemplateService redisTemService;

	@Autowired
	UserService userService;

	@Override
	public Response signUpApiCall(String firstName, String mobileNo, String domainurl, String info1) {

		AppUser appUser = null;
		Response resObj = new Response();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			String url = Constants.SIGNUP_API_URL + "api/v5/user/web_social_login";
			String email = mobileNo;

			// String deviceId = mobileNo + "_web";

			String deviceId = "PG_" + RandomStringUtils.randomAlphanumeric(8) + "_"
					+ String.valueOf(new Date().getTime());

			logger.info("device id is...." + deviceId + " length...." + deviceId.length());

			// String POST_PARAMS =
			// "{\"version\":\"Qureka_1.0\",\"email\":\"akashnain93@gmail.com\",\"mobile\":\"\",\"deviceid\":\"1233@45#\",\"firstName\":\"Akash\",\"gaid\":\"\",\"info1\":\"Facebook\",\"info3\":\"Qureka\",\"lang\":\"en\",\"lastName\":\"Nain\",\"manufacturer\":\"\",\"model\":\"\",\"serialno\":\"\",\"fcmid\":\"kjhkjhkjjhk\",\"active\":1}";

			String POST_PARAMS = "{\"version\":\"predguru\",\"firstName\":\"" + firstName + "\",\"email\":\""
					+ email + "\",\"deviceid\":\"" + deviceId + "\",\"info1\":\"" + info1 + "\",\"info3\":\""
					+ domainurl + "\"}";

			System.out.println("Yes I am coming ");
			logger.info("post request generated...." + POST_PARAMS);

			String hashkey = DynamicApiKeyConstant.REGISTER_ENCRYPT_VALUE;

			String cipherText = AESCrypto.encryptionUtil(hashkey, POST_PARAMS.getBytes());

			logger.info("encrypted value...." + cipherText);
			String decryptedCipherText = AESCrypto.decryptUtils(hashkey, cipherText);

			logger.info("decrypted value...." + decryptedCipherText);
			StringEntity entity = new StringEntity(cipherText);

			HttpUriRequest httppost = RequestBuilder.post().setUri(new URI(url))
					.setEntity(EntityBuilder.create().setText(cipherText).build()).build();

			CloseableHttpResponse response = httpclient.execute(httppost);
			System.out.println(response.toString());
			logger.info("response...." + response.toString());
			try {

				if (response.getStatusLine().getStatusCode() == 200) {

					String appUserStr = String.valueOf(EntityUtils.toString(response.getEntity()));
					logger.info("response value when status is 200...." + appUserStr);

					ObjectMapper objectMapper = new ObjectMapper();
					appUser = objectMapper.readValue(appUserStr, AppUser.class);

					resObj.setMessage("success");
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject(appUser);
					return resObj;

				} else {
					String message = String.valueOf(EntityUtils.toString(response.getEntity()));

					resObj.setMessage(message);
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject(appUser);
					return resObj;
				}
			} catch (Exception e) {

			}

			httpclient.close();
		} catch (Exception e) {
		}

		return resObj;
	}

	@Override
	public Response verifyApiCall(String mobileNo, String otp) {

		AppUser appUser = null;
		Response resObj = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		try {

			String url = Constants.API_URL + "api/v4/user/verify";

			String POST_PARAMS = "{\"mobileNo\":\"" + mobileNo + "\",\"otp\":" + otp + "}";
			String hashkey = DynamicApiKeyConstant.OTP_ENCRYPT_VALUE;

			String cipherText = AESCrypto.encryptionUtil(hashkey, POST_PARAMS.getBytes());

			String decryptedCipherText = AESCrypto.decryptUtils(hashkey, cipherText);

			HttpUriRequest httppost = RequestBuilder.post().setUri(new URI(url)).addParameter("data", cipherText)
					.build();

			CloseableHttpResponse response = httpclient.execute(httppost);

			try {

				if (response.getStatusLine().getStatusCode() == 200) {

					String appUserStr = String.valueOf(EntityUtils.toString(response.getEntity()));

					ObjectMapper objectMapper = new ObjectMapper();
					appUser = objectMapper.readValue(appUserStr, AppUser.class);

					resObj.setMessage("success");
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject(appUser);
					return resObj;

				} else {
					String message = String.valueOf(EntityUtils.toString(response.getEntity()));

					resObj.setMessage(message);
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject(appUser);
					return resObj;
				}
			} catch (Exception e) {

			}

			httpclient.close();
		} catch (Exception e) {
		}

		return resObj;

	}

	@Override
	public Response minusCoinApiCall(String userId, Long quizId, int entryFees, String type, String appName) {
		// userId, coins, reason, gameType, appName

		Response resObj = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		try {
			String res = "";
			String url = Constants.API_URL + "api/v1/user/minusCoins";
			if (type == "predection") {
				res = "prediction_joined-" + quizId;

			}
			HttpUriRequest httppost = RequestBuilder.post().setUri(new URI(url)).addParameter("userId", userId)
					.addParameter("coins", "" + entryFees).addParameter("reason", res).addParameter("gameType", "1")
					.addParameter("appName", appName).build();

			CloseableHttpResponse response = httpclient.execute(httppost);

			try {

				if (response.getStatusLine().getStatusCode() == 200) {

					String appUserStr = String.valueOf(EntityUtils.toString(response.getEntity()));
					resObj.setMessage("success");
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject("");
					return resObj;

				} else {
					String message = String.valueOf(EntityUtils.toString(response.getEntity()));
					resObj.setMessage(message);
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject("");
					return resObj;
				}
			} catch (Exception e) {

			}

			httpclient.close();
		} catch (Exception e) {
		}

		return resObj;
	}

	@Override
	public Response addCoinsApiCall(String userId, long coin, String appName, String res) {

		Response resObj = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		try {

			String url = Constants.API_URL + "api/v1/user/addCoins";

			// String res = "predGuru_web";
			HttpUriRequest httppost = RequestBuilder.post().setUri(new URI(url)).addParameter("userId", userId)
					.addParameter("coins", "" + coin).addParameter("reason", res).addParameter("appName", appName)
					.build();

			CloseableHttpResponse response = httpclient.execute(httppost);
			try {
				if (response.getStatusLine().getStatusCode() == 200) {

					String appUserStr = String.valueOf(EntityUtils.toString(response.getEntity()));

					resObj.setMessage("success");
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject("");
					return resObj;

				} else {
					String message = String.valueOf(EntityUtils.toString(response.getEntity()));
					resObj.setMessage(message);
					resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
					resObj.setObject("");
					return resObj;
				}
			} catch (Exception e) {

			}

			httpclient.close();
		} catch (Exception e) {
		}

		return resObj;

	}

	@Override
	public Response updateToken(AppUser user) {

		Response response = new Response();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v4/user/updateUserDetail";
			//,\"email\":\""+user.getEmail()+"\"
			String POST_PARAMS = "{\"id\":\""+user.getId()+"\",\"fcmid\":\""+user.getFcmid()+"\",\"info3\":\""+user.getInfo3()+"\"}";


			String hashkey = DynamicApiKeyConstant.UPDATE_PROFILE_ENCRYPT_VALUE;

			String cipherText = AESCrypto.encryptionUtil(hashkey, POST_PARAMS.getBytes());

			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("data", cipherText)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {
					response.setMessage(String.valueOf(EntityUtils.toString(httpResponse.getEntity())));
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject("");
					return response;
				}else {
					response.setMessage(String.valueOf(EntityUtils.toString(httpResponse.getEntity())));
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject("");
					return response;
				}
			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}

		return response;

	}


}
