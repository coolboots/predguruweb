
package com.coolboots.qureka.predGuru.user.model.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Created by sachin on 7/3/16.
 */
public class CustomJsonSerializer extends JsonSerializer<JsonInterface> {
    @Override
    public void serialize(JsonInterface value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
//        jgen.writeString(""+value.getId());
        jgen.writeString(value.returnJsonKey());
    }
}