package com.coolboots.qureka.predGuru.user.model;

import java.util.Date;
import java.util.Set;


import org.springframework.format.annotation.NumberFormat;


public class Wallet {
	
	
	private Long id;
	

	private String appUser;
	
	private Date createdOn;

	private Date updatedOn;

	private String details;

	private Boolean active;
	
    private Double totalAmount;

    private Double currentBalance;
    
    private Long walletDetails;

	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getAppUser() {
		return appUser;
	}

	public void setAppUser(String appUser) {
		this.appUser = appUser;
	}
	
	public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    @NumberFormat(pattern=".##")
    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }


	@NumberFormat(pattern=".##")
    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }
    
    
	public Long getWalletDetails() {
		return walletDetails;
	}

	public void setWalletDetails(Long walletDetails) {
		this.walletDetails = walletDetails;
	}

	@Override
	public String toString() {
		return "Wallet [id=" + id + ", appUser=" + appUser + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
				+ ", details=" + details + ", active=" + active + ", totalAmount=" + totalAmount + ", currentBalance="
				+ currentBalance + ", walletDetails=" + walletDetails + "]";
	}


    
  
}
