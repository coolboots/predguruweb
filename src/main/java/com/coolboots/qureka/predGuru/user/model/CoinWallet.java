package com.coolboots.qureka.predGuru.user.model;

import java.util.Date;
import java.util.Set;




public class CoinWallet {

    private Long id;


    private String appUser; 
    
    private Date createdOn;

    private Date updatedOn;

    private String details;

    private Boolean active;

    private Integer currentBalance;

    private Integer totalCoins;


    private Long coinsDetails;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    

    public String getAppUser() {
		return appUser;
	}

	public void setAppUser(String appUser) {
		this.appUser = appUser;
	}
    

    public Long getCoinsDetails() {
		return coinsDetails;
	}

	public void setCoinsDetails(Long coinsDetails) {
		this.coinsDetails = coinsDetails;
	}

	public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Integer currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Integer getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(Integer totalCoins) {
        this.totalCoins = totalCoins;
    }

	@Override
	public String toString() {
		return "CoinWallet [id=" + id + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
				+ ", details=" + details + ", active=" + active + ", currentBalance=" + currentBalance + ", totalCoins="
				+ totalCoins + ", coinsDetails=" + coinsDetails + "]";
	}

	
}