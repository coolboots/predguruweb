package com.coolboots.qureka.predGuru.user.model;

import java.io.Serializable;
import java.util.Date;




public class AppUserDetails {
	
	Long id;
	Date addedOn;
	Date lastRedeemDate;
	Date refLastRedeemDate;
	boolean userThrashHold;
	int gamesCount;
	int refGameCount;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public Date getLastRedeemDate() {
		return lastRedeemDate;
	}
	public void setLastRedeemDate(Date lastRedeemDate) {
		this.lastRedeemDate = lastRedeemDate;
	}
	public boolean getUserThrashHold() {
		return userThrashHold;
	}
	public void setUserThrashHold(boolean userThrashHold) {
		this.userThrashHold = userThrashHold;
	}
	public int getGamesCount() {
		return gamesCount;
	}
	public void setGamesCount(int gamesCount) {
		this.gamesCount = gamesCount;
	}
	
	public Date getRefLastRedeemDate() {
		return refLastRedeemDate;
	}
	public void setRefLastRedeemDate(Date refLastRedeemDate) {
		this.refLastRedeemDate = refLastRedeemDate;
	}
	public int getRefGameCount() {
		return refGameCount;
	}
	public void setRefGameCount(int refGameCount) {
		this.refGameCount = refGameCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addedOn == null) ? 0 : addedOn.hashCode());
		result = prime * result + gamesCount;
		result = prime * result
				+ ((lastRedeemDate == null) ? 0 : lastRedeemDate.hashCode());
		result = prime * result + (userThrashHold ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppUserDetails other = (AppUserDetails) obj;
		if (addedOn == null) {
			if (other.addedOn != null)
				return false;
		} else if (!addedOn.equals(other.addedOn))
			return false;
		if (gamesCount != other.gamesCount)
			return false;
		if (lastRedeemDate == null) {
			if (other.lastRedeemDate != null)
				return false;
		} else if (!lastRedeemDate.equals(other.lastRedeemDate))
			return false;
		if (userThrashHold != other.userThrashHold)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AppUserDetails [lastRedeemDate="
				+ lastRedeemDate + ", userThrashHold=" + userThrashHold
				+ ", gamesCount=" + gamesCount + "]";
	}
	
	
}

