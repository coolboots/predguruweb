package com.coolboots.qureka.predGuru.user.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AppUser {
	
    private String id;

    private String oldId;

    private Long wallet;

    private Long coinWallet;
    
    private Long referralWallet;

    private String firstName;

    private String lastName;

    private String email;

    private String mobile;

    private String deviceid;

    private Date addedOn=new Date();
    
    private Boolean imported;

    private String status;
    
    private Boolean active;

    private String version;

    private Date lastUpdateOn;

    private String fcmid;

    private String displayName;

    private String gaid;

    private String city;

    private String lang;

    private String info1;

    private String info2;

    private String info3;

    private String info4;

    
    private String referenceCode;

    private Long appUserDetails;

    private Boolean notification = true;

    private String serialno;

    private Date flikkUserDate;
    
    private String manufacturer;
    
    private String model;

    private Boolean blocked;
    
    private String userTag;
    
    private String pan;
    
    private String profileImage;
    
    

    @JsonProperty("profile_image")
    public String getProfileImage() {
		return profileImage;
	}



	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}



	public Boolean getActive() {
		return active;
	}



	public AppUser(){

    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	
	public Long getWallet() {
		return wallet;
	}

	public void setWallet(Long wallet) {
		this.wallet = wallet;
	}


	public Long getCoinWallet() {
		return coinWallet;
	}

	public void setCoinWallet(Long coinWallet) {
		this.coinWallet = coinWallet;
	}
	
	
	public Long getReferralWallet() {
		return referralWallet;
	}

	public void setReferralWallet(Long referralWallet) {
		this.referralWallet = referralWallet;
	}

	public String getVersion() {
        return version;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }

    public String getDisplayName() {
        return displayName;
    }


    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

    public String getFcmid() {
        return fcmid;
    }

    public void setFcmid(String fcmid) {
        this.fcmid = fcmid;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    
    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Boolean getImported() {
		return imported;
	}

	public void setImported(Boolean imported) {
		this.imported = imported;
	}


    
    public Date getFlikkUserDate() {
        return flikkUserDate;
    }

    public void setFlikkUserDate(Date flikkUserDate) {
        this.flikkUserDate = flikkUserDate;
    }

    public Date getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(Date lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getGaid(){
        return gaid;
    }
    public void setGaid(String gaid){
        this.gaid=gaid;
    }

    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }

    public String getLang() {
        return lang;
    }


    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getInfo1() {
        return info1;
    }


    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    public String getInfo2() {
        return info2;
    }


    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    public String getInfo3() {
        return info3;
    }


    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    public String getInfo4() {
        return info4;
    }


    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

    public Boolean getBlocked() {
        return this.blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }
    
    public Long getAppUserDetails() {
        return appUserDetails;
    }

    public void setAppUserDetails(Long appUserDetails) {
        this.appUserDetails = appUserDetails;
    }
    
    public String getUserTag() {
		return userTag;
	}

	public void setUserTag(String userTag) {
		this.userTag = userTag;
	}
	
	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}



	@Override
	public String toString() {
		return "AppUser [id=" + id + ", oldId=" + oldId + ", wallet=" + wallet + ", coinWallet=" + coinWallet
				+ ", referralWallet=" + referralWallet + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", mobile=" + mobile + ", deviceid=" + deviceid + ", addedOn=" + addedOn
				+ ", imported=" + imported + ", status=" + status + ", active=" + active + ", version=" + version
				+ ", lastUpdateOn=" + lastUpdateOn + ", fcmid=" + fcmid + ", displayName=" + displayName + ", gaid="
				+ gaid + ", city=" + city + ", lang=" + lang + ", info1=" + info1 + ", info2=" + info2 + ", info3="
				+ info3 + ", info4=" + info4 + ", referenceCode=" + referenceCode + ", appUserDetails=" + appUserDetails
				+ ", notification=" + notification + ", serialno=" + serialno + ", flikkUserDate=" + flikkUserDate
				+ ", manufacturer=" + manufacturer + ", model=" + model + ", blocked=" + blocked + ", userTag="
				+ userTag + ", pan=" + pan + ", profileImage=" + profileImage + "]";
	}

	

}