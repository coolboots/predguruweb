package com.coolboots.qureka.predGuru.user.api;

import com.coolboots.qureka.predGuru.handler.Response;

public interface WalletApiService {

	Response wallet(String id);

}
