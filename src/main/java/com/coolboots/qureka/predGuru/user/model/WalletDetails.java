package com.coolboots.qureka.predGuru.user.model;

import java.util.Date;


import org.springframework.format.annotation.NumberFormat;


public class WalletDetails {

  
    private Long id;

    private Long wallet;
    
    private String mode;
    
    private String activityDetails;
    
    private Date date;
    
    @NumberFormat(pattern=".##")
    private Double amount;
    
    private String reason;
    
    private String appName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWallet() {
		return wallet;
	}

	public void setWallet(Long wallet) {
		this.wallet = wallet;
	}

	public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getActivityDetails() {
        return activityDetails;
    }

    public void setActivityDetails(String activityDetails) {
        this.activityDetails = activityDetails;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
    
    public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
    public String toString() {
        return "WalletDetails{" +
                "id=" + id +
                ", mode='" + mode + '\'' +
                ", activityDetails='" + activityDetails + '\'' +
                ", date=" + date +
                ", amount=" + amount +
                ", reason=" + reason +
                '}';
    }

	
    
}
