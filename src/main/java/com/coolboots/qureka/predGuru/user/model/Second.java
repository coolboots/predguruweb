package com.coolboots.qureka.predGuru.user.model;

public class Second {
	private Integer denomination;
	private Integer gameCount;
	private Integer redemptionType;
	
	
	public Integer getDenomination() {
		return denomination;
	}
	public void setDenomination(Integer denomination) {
		this.denomination = denomination;
	}
	public Integer getGameCount() {
		return gameCount;
	}
	public void setGameCount(Integer gameCount) {
		this.gameCount = gameCount;
	}
	public Integer getRedemptionType() {
		return redemptionType;
	}
	public void setRedemptionType(Integer redemptionType) {
		this.redemptionType = redemptionType;
	}
	@Override
	public String toString() {
		return "Second [denomination=" + denomination + ", gameCount=" + gameCount + ", redemptionType="
				+ redemptionType + "]";
	}
	
	
}
