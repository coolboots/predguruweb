package com.coolboots.qureka.predGuru.api.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.PrizeMatrix;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PredictionApiServiceImpl implements PredictionApiService {

	@Autowired
	RedisTemplateService redisTemplateService;

	@Override
	public void matchApiCall() {

		List<MatchEventPlainPojo> matchEventPlainPojoList;

		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v2/matches";

			HttpUriRequest httppost = RequestBuilder.get()
					.setUri(new URI(url))
					.addParameter("lang", "en")
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					redisTemplateService.writeToRedis("matchEventen", ""+obj, 10l, TimeUnit.MINUTES);

				}

			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}


	}

	@Override
	public Response matchPreizeMatrixApi(Long matchId) {

		Response response = new Response();

		List<PrizeMatrix> prizeMatrixList = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v2/prize_matrix?matchId="+matchId;
			HttpUriRequest httppost = RequestBuilder.get()
					.setUri(new URI(url))
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			try {

				if(httpResponse.getStatusLine().getStatusCode()== 200) {

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));

					ObjectMapper objectMapper = new ObjectMapper();
					prizeMatrixList =	objectMapper.readValue(obj, new TypeReference<ArrayList<PrizeMatrix>>() {});

					response.setMessage(obj);
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject(prizeMatrixList);

					return response;

				}else {
					String message = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					response.setMessage(message);
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject("");

					return response;
				}

			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}
		return null;

	}

	@Override
	public Response predictionJoinApiCall(Long matchId, Long quesId, String userId, String givenAns,String appName) {

		Response resObj = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		if(userId ==null) {
			resObj.setStatus(200);
		}
		else if(userId != null) {

			try {

				String userString = "{\"appUserId\": \""+userId+"\", \"matchId\": "+matchId+", \"questionId\": "+quesId+",\"givenOption\": \""+givenAns+"\",\"appName\": \""+appName+"\"}";


				StringEntity entity = new StringEntity(userString,
						ContentType.APPLICATION_JSON);

				HttpClient httpClient = HttpClientBuilder.create().build();
				HttpPost request = new HttpPost(Constants.API_URL+"api/v2/match/answer");
				request.setEntity(entity);

				HttpResponse response = httpClient.execute(request);

				try {

					if(response.getStatusLine().getStatusCode()== 200) {

						String joinRespo = String.valueOf(EntityUtils.toString(response.getEntity()));

						resObj.setMessage(joinRespo);
						resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
						resObj.setObject("");
						return resObj;

					}else if(response.getStatusLine().getStatusCode()== 208) {
						Boolean joinRespo = Boolean.valueOf(EntityUtils.toString(response.getEntity()));
						resObj.setMessage(""+joinRespo);
						resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
						resObj.setObject("");
						return resObj;
					}else{
						String message = String.valueOf(EntityUtils.toString(response.getEntity()));

						resObj.setMessage(message);
						resObj.setStatus(Integer.valueOf(response.getStatusLine().getStatusCode()));
						resObj.setObject("");
						return resObj;
					}
				}catch (Exception e) {

				}

				httpclient.close();
			}catch (Exception e) {} 
		}
		return resObj;
	}

	@Override
	public Response matchJoinningApiCall(String userId, Long matchId,String appName) {

		Response response = new Response();

		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v1/match/joinning";
			HttpUriRequest httppost = RequestBuilder.post()
					.setUri(new URI(url))
					.addParameter("matchId", matchId+"")
					.addParameter("userId", userId)
					.addParameter("appName", appName)
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			
			System.out.println("httpResponse : "+httpResponse);
			
			try {


				if(httpResponse.getStatusLine().getStatusCode()== 200) {

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));

					response.setMessage("");
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject(obj);
					return response;

				}else  if(httpResponse.getStatusLine().getStatusCode()== 208){

					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));

					response.setMessage("");
					response.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
					response.setObject(obj);
					return response;
				}

			} finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}
		return response;
	}

	@Override
	public void userStatsAPiCall(Long matchId) {

		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			String url = Constants.API_URL+"api/v1/answer/stats/"+matchId;
			HttpUriRequest httppost = RequestBuilder.get()
					.setUri(new URI(url))
					.build();

			CloseableHttpResponse httpResponse = httpclient.execute(httppost);
			try {
				if(httpResponse.getStatusLine().getStatusCode()== 200) {
					String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
					redisTemplateService.writeHashValueToField("userstats", ""+matchId, obj);
				}

			}catch (JsonParseException e) {
			}catch (JsonMappingException e) {
			}catch (Exception e) {} 
			finally {
				httpResponse.close();
				httpclient.close();
			}
		}catch (Exception e) {}
	}

}
