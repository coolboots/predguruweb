package com.coolboots.qureka.predGuru.api.service;

import com.coolboots.qureka.predGuru.handler.Response;

public interface PredictionApiService {

	void matchApiCall();

	Response matchPreizeMatrixApi(Long matchId);

	Response predictionJoinApiCall(Long matchId, Long quesId, String userId, String givenAns,String appName);

	Response matchJoinningApiCall(String userId, Long matchId,String appName);

	void userStatsAPiCall(Long matchId);

}
