package com.coolboots.qureka.predGuru.dto;

import org.springframework.format.annotation.NumberFormat;

public class OtpDto {
	
	@NumberFormat
   // @NotEmpty
    private String otp;
    
   public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
