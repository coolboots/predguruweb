package com.coolboots.qureka.predGuru.dto;

public class WalletDto {

	private Double walletBalanec;
	private Double referralBalance;
	private int coinBalance;
	
	
	public Double getWalletBalanec() {
		return walletBalanec;
	}
	public void setWalletBalanec(Double walletBalanec) {
		this.walletBalanec = walletBalanec;
	}
	public Double getReferralBalance() {
		return referralBalance;
	}
	public void setReferralBalance(Double referralBalance) {
		this.referralBalance = referralBalance;
	}
	public int getCoinBalance() {
		return coinBalance;
	}
	public void setCoinBalance(int coinBalance) {
		this.coinBalance = coinBalance;
	}
	@Override
	public String toString() {
		return "WalletDto [walletBalanec=" + walletBalanec + ", referralBalance=" + referralBalance + ", coinBalance="
				+ coinBalance + "]";
	}
	
	
}
