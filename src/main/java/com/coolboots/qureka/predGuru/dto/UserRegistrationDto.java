package com.coolboots.qureka.predGuru.dto;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;



public class UserRegistrationDto {

    @NotEmpty
    private String firstName;
    
    private String info3;
    
    private String info1;
    
    @NumberFormat
    @NotEmpty
    private String mobile;
    
    private String email;
    
    
    
    @NumberFormat
    @NotEmpty
    private String otp;
    
    public String getInfo3() {
		return info3;
	}

	public void setInfo3(String info3) {
		this.info3 = info3;
	}

	@AssertTrue
    private Boolean terms;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInfo1() {
		return info1;
	}

	public void setInfo1(String info1) {
		this.info1 = info1;
	}
    
    
}
