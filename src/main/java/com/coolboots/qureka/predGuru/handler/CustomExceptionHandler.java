package com.coolboots.qureka.predGuru.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Response> handelException(CustomNotFoundException ex){

        Response er = new Response();
        er.setMessage(ex.getMessage());
        /*er.setStatus(HttpStatus.BAD_REQUEST.value());
        er.setTimeStamp(System.currentTimeMillis());
*/
        return new ResponseEntity<>(er, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<Response> handelException(Exception e){
        Response errorResponse = new Response();

        /*errorResponse.setTimeStamp(System.currentTimeMillis());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        *///errorResponse.setMessage("Somthing went wrong, Please check ");
        errorResponse.setMessage(e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

    }

}
