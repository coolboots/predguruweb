package com.coolboots.qureka.predGuru.handler;

public class Response {

    private int status;
    private String message;
    private Object object;

    public Response(){

    }

    public Response(String message, Object object){
        this.message=message;
    }
    public Response(String message){
        this.message=message;
    }
    
    public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", object=" + object + "]";
	}

    
    
}
