package com.coolboots.qureka.predGuru.handler;

import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

public class CustomArgumentNotValidException extends MethodArgumentNotValidException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomArgumentNotValidException(MethodParameter parameter, BindingResult bindingResult) {
		super(parameter, bindingResult);
		
	}

	
	@Override
	public String getMessage() {
		
//		StringBuilder sb =
		
		/*for(ObjectError objectError:this.getBindingResult().getAllErrors()) {
			
		}*/
		StringBuilder sb = new StringBuilder();
		for (ObjectError error : this.getBindingResult().getAllErrors()) {
			sb.append("[").append(error).append("] ");
		}
		
		
		return sb.toString();
	}
	
}
