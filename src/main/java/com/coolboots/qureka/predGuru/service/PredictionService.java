package com.coolboots.qureka.predGuru.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.QuestionPlainPojo;
import com.coolboots.qureka.predGuru.model.SampleMatchPojo;

public interface PredictionService {

	MatchEventPlainPojo getMatchList(Long matchId);

	//Response predictionJoinApiCall(UserAnswer userAnswer);
	
	public List<QuestionPlainPojo> getPredictionQuestion(Long matchId);

	String nextQuestionEncrpt(Long matchId, int size, int size2, QuestionPlainPojo questionPlainPojo);

	String getUserStatsByMatch(Long matchId, QuestionPlainPojo questionPlainPojo);

	List<MatchEventPlainPojo> getMatchList(HttpServletRequest request);

	String getJoinEncrypt(Long matchId, long entryAmount);

	public List<QuestionPlainPojo> getPredictionQuestion(HttpServletRequest request);

	SampleMatchPojo getSampleQuestions();
}
