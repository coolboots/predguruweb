package com.coolboots.qureka.predGuru.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.UserRank;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RecentWinnerServiceImpl implements RecentWinnerService{

@Autowired RedisTemplateService redisTemplateService;
		
@Autowired
PredictionService preService;
		@Override
		public Response recentPredictionWinnerApiCall(Long matchId) {
			
			Response resObj = new Response();
			CloseableHttpClient httpclient = HttpClients.createDefault();
			try {
				
				String url = Constants.API_URL+"api/v1/rank/result?matchId="+matchId;
				
				
				HttpUriRequest httppost = RequestBuilder.get()
						.setUri(new URI(url)).build();

 				CloseableHttpResponse httpResponse = httpclient.execute(httppost);
				
				try {
					
					if(httpResponse.getStatusLine().getStatusCode()== 200) {
										
						
						String obj = String.valueOf(EntityUtils.toString(httpResponse.getEntity()));
						
						ObjectMapper objectMapper = new ObjectMapper();

						List<UserRank> predictionWinner = objectMapper.readValue(obj, new TypeReference<ArrayList<UserRank>>() {});
					    resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
						resObj.setObject(predictionWinner);
						resObj.setMessage("success");

					}else {
						resObj.setStatus(Integer.valueOf(httpResponse.getStatusLine().getStatusCode()));
						resObj.setObject("");
						resObj.setMessage("fail");
					}
				} finally {
					httpResponse.close();
					httpclient.close();
				}
			}catch (Exception e) {e.printStackTrace();}
			return resObj;	
		}
	
		@Override
		public Collection<MatchEventPlainPojo> finalResults(HttpServletRequest request) {

			
			List<MatchEventPlainPojo>finalResults = new ArrayList<>();

			List<MatchEventPlainPojo> resultList = preService.getMatchList(request);
			try {
			List<MatchEventPlainPojo> homeResults = resultList.stream().filter(fixture ->
			fixture.getMatchCompleted().equals(true))
					.collect(Collectors.toList());
			finalResults.addAll(homeResults);
			}catch (Exception e) {
				e.printStackTrace();
			}
			return finalResults;
		}
}
