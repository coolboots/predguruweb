package com.coolboots.qureka.predGuru.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coolboots.qureka.predGuru.Constants;
import com.coolboots.qureka.predGuru.CookieConfig;
import com.coolboots.qureka.predGuru.RedisTemplateService;
import com.coolboots.qureka.predGuru.api.service.PredictionApiService;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;
import com.coolboots.qureka.predGuru.model.QuestionPlainPojo;
import com.coolboots.qureka.predGuru.model.QuestionSetPlainPojo;
import com.coolboots.qureka.predGuru.model.SampleMatchPojo;
import com.coolboots.qureka.predGuru.model.SampleQuestionPojo;
import com.coolboots.qureka.predGuru.model.UserAnswerStat;
import com.coolboots.qureka.predGuru.utils.AESCrypto;
import com.coolboots.qureka.predGuru.utils.DynamicApiKeyConstant;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PredictionServiceImpl implements PredictionService {

	@Autowired
	RedisTemplateService redisTemService;

	@Autowired
	PredictionApiService predictionApiService;

	@Autowired
	PredictionService predictionService;

	@Override
	public MatchEventPlainPojo getMatchList(Long matchId) {

		String matchStr = redisTemService.readFromRedis("matchEventen");

		MatchEventPlainPojo matchQui = null;

		if (!matchStr.equals("")) {

			try {
				List<MatchEventPlainPojo> matchs = null;
				ObjectMapper objectMapper = new ObjectMapper();
				matchs = objectMapper.readValue(matchStr, new TypeReference<ArrayList<MatchEventPlainPojo>>() {
				});
				for (MatchEventPlainPojo matc : matchs) {
					if (matchId.equals(matc.getId())) {
						return matc;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return matchQui;
	}

	@Override
	public List<QuestionPlainPojo> getPredictionQuestion(Long matchId) {

		try {
			List<QuestionPlainPojo> questionList = null;
			List<MatchEventPlainPojo> matchList = null;

			String matchListStr = redisTemService.readFromRedis("matchEventen");

			if (!matchListStr.equals("")) {
				ObjectMapper objectMapper = new ObjectMapper();
				matchList = objectMapper.readValue(matchListStr, new TypeReference<ArrayList<MatchEventPlainPojo>>() {
				});
			}

			if (matchList != null) {
				for (MatchEventPlainPojo mat : matchList) {
					if (mat.getId().equals(matchId)) {
						List<QuestionSetPlainPojo> mm = mat.getQuestionSetPojos();
						for (QuestionSetPlainPojo mk : mm) {
							questionList = mk.getQuestions();
							return questionList;
						}
					}

				}
			}

			return questionList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String nextQuestionEncrpt(Long matchId, int size, int queSize, QuestionPlainPojo questionPlainPojo) {

		try {
//			Question question = new Question();

			String quesString = matchId + "-" + size + "-" + queSize + "-" + questionPlainPojo.getId();

			return quesString;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getUserStatsByMatch(Long matchId, QuestionPlainPojo que) {

		try {

			String redisQues = redisTemService.readHashValueFromField("userstats", "" + matchId);

			if (!redisQues.equals("")) {
				ObjectMapper objectMapper = new ObjectMapper();
				List<UserAnswerStat> userStatsList = objectMapper.readValue(redisQues,
						new TypeReference<ArrayList<UserAnswerStat>>() {
						});
				if (!CollectionUtils.isEmpty(userStatsList)) {
					for (UserAnswerStat userAnswerStat : userStatsList) {

						if (userAnswerStat.getQuestionId().equals(que.getId())) {
							String userAnsStats = objectMapper.writeValueAsString(userAnswerStat);
							return userAnsStats;
						}
					}
				}
			}
		} catch (Exception e) {
		}

		return null;
	}

	@Override
	public List<MatchEventPlainPojo> getMatchList(HttpServletRequest request) {

		String userId = CookieConfig.getCookies(request, "userId");

		List<MatchEventPlainPojo> list = new ArrayList<>();

		ObjectMapper objectMapper = new ObjectMapper();

		String matchListStr = redisTemService.readFromRedis("matchEventen");

		if (matchListStr.equals("") || matchListStr.equals("[]")) {
			predictionApiService.matchApiCall();
			matchListStr = redisTemService.readFromRedis("matchEventen");
		}
		if (!matchListStr.equals("")) {
			try {
				List<MatchEventPlainPojo> matchList = null;
				matchList = objectMapper.readValue(matchListStr, new TypeReference<ArrayList<MatchEventPlainPojo>>() {
				});

				Date date = new Date();
				for (MatchEventPlainPojo po : matchList) {

					if (date.before(po.getStartDate())) {
						Long matchId = po.getId();
						String userJoin = redisTemService.readHashValueFromField("match_score_" + userId, "" + matchId);
						if (userJoin.equals("success")) {
							po.setPlayed(userJoin);
						} else if(request.getSession().getAttribute(matchId + "_" + userId + "question_size") != null){
							po.setPlayed("pending");
						}else {
							po.setPlayed("failure");
						}

						if (po.getEntryAmount() == -1) {
							po.setEntryAmount(Constants.WATCH_VIDEO_COIN);
						}

						Date startTime = po.getStartDate();
						long nn = startTime.getTime();
						Calendar cal = Calendar.getInstance();
						cal.setTimeInMillis(nn);
						String timeString = new SimpleDateFormat("d MMMM,hh:mm a").format(cal.getTime());
						po.setTimeStamp(timeString);

						list.add(po);
					} else {

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	@Override
	public String getJoinEncrypt(Long matchId, long entryAmount) {

		try {
			String hashkey = DynamicApiKeyConstant.PREDICTION_JOIN_ENCRYPT_VALUE;

			String quesString = matchId + "-" + entryAmount;
			String cipherText;
			cipherText = AESCrypto.encryptionUtil(hashkey, quesString.getBytes());
//			String decryptedCipherText = AESCrypto.decryptUtils(hashkey, cipherText);
			return cipherText;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<QuestionPlainPojo> getPredictionQuestion(HttpServletRequest request) {

		try {
			List<QuestionPlainPojo> questionList = null;
			List<MatchEventPlainPojo> matchList = null;
			matchList = predictionService.getMatchList(request);

			if (matchList != null) {
				for (MatchEventPlainPojo mat : matchList) {
					List<QuestionSetPlainPojo> mm = mat.getQuestionSetPojos();
					for (QuestionSetPlainPojo mk : mm) {
						questionList = mk.getQuestions();
						return questionList;
					}
				}
			}
			return questionList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SampleMatchPojo getSampleQuestions() {

		ObjectMapper objectMapper = new ObjectMapper();
		List<SampleQuestionPojo> questions = new ArrayList<>();
		MatchEventPlainPojo match = new MatchEventPlainPojo();
		SampleMatchPojo sampleMatchPojo = new SampleMatchPojo();

		String matchListStr = redisTemService.readFromRedis("matchEventen");

		if (matchListStr.equals("") || matchListStr.equals("[]")) {
			predictionApiService.matchApiCall();
			matchListStr = redisTemService.readFromRedis("matchEventen");
		}

		if (!matchListStr.equals("")) {
			try {
				List<MatchEventPlainPojo> matchList = objectMapper.readValue(matchListStr,
						new TypeReference<ArrayList<MatchEventPlainPojo>>() {
						});

				Date date = new Date();
				// get the next match from the list
//				match = matchList.stream().filter(m -> m.getStartDate().after(date)).limit(1).findAny().get();

				match = matchList.stream().filter(m -> m.getStartDate().after(date) && m.getSingleTeam() == false)
						.max(Comparator.comparing(MatchEventPlainPojo::getCoins)).orElseThrow(NoSuchElementException::new);

				Long matchId = match.getId();

				// get questions for given match
				List<QuestionPlainPojo> allquestionsPlainPojo = match.getQuestionSetPojos().get(0).getQuestions();

				List<QuestionPlainPojo> questionsPlainPojo	=allquestionsPlainPojo.stream().filter( q -> StringUtils.isEmpty(q.getOptionD())).collect(Collectors.toList());
				int questionsSize = questionsPlainPojo.size();
				// minimum questions must be 2
				if(questionsSize<2) {
					//if questions less than 2 add two more questions.
					questionsPlainPojo.add(allquestionsPlainPojo.get(0));
					questionsPlainPojo.add(allquestionsPlainPojo.get(1));
					questionsSize=2;
				}

				// get two random question from the list and set values to SampleQuestionPojo.
				Random rand = new Random();
				int firstRandom = rand.nextInt(questionsSize);

				SampleQuestionPojo question1 = new SampleQuestionPojo(questionsPlainPojo.get(firstRandom));
				System.out.println(firstRandom + "-" + question1.getQuestionId());
				int secondRandom = firstRandom;
				while (firstRandom == secondRandom) {
					secondRandom = rand.nextInt(questionsSize);
				}

				SampleQuestionPojo question2 = new SampleQuestionPojo(questionsPlainPojo.get(secondRandom));
				System.out.println(secondRandom + "-" + question2.getQuestionId());
				System.out.println("--------------------------");

				// get stats map
				Map<Long, UserAnswerStat> statsMap = getQuestionStatsMap(matchId);
				if (statsMap != null) {
					UserAnswerStat stats1 = statsMap.get(question1.getQuestionId());
					question1.setStats(objectMapper.writeValueAsString(stats1));

					UserAnswerStat stats2 = statsMap.get(question2.getQuestionId());
					question2.setStats(objectMapper.writeValueAsString(stats2));
				}
				questions.add(question1);
				questions.add(question2);

				sampleMatchPojo.setMatchId(match.getId());
				sampleMatchPojo.setTeamA(match.getTeamA().getShortName());
				sampleMatchPojo.setTeamAUrl(match.getTeamA().getFlagUrl());
				sampleMatchPojo.setTeamB(match.getTeamB().getShortName());
				sampleMatchPojo.setTeamBUrl(match.getTeamB().getFlagUrl());
				sampleMatchPojo.setQuestions(questions);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sampleMatchPojo;
	}

	private Map<Long, UserAnswerStat> getQuestionStatsMap(Long matchId) {
		try {
			String redisQues = redisTemService.readHashValueFromField("userstats", "" + matchId);
			if (!redisQues.equals("")) {
				ObjectMapper objectMapper = new ObjectMapper();
				List<UserAnswerStat> userStatsList = objectMapper.readValue(redisQues,
						new TypeReference<ArrayList<UserAnswerStat>>() {
						});
				if (!CollectionUtils.isEmpty(userStatsList)) {
					Map<Long, UserAnswerStat> map = userStatsList.stream()
							.collect(Collectors.toMap(UserAnswerStat::getQuestionId, stat -> stat));

					return map;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
