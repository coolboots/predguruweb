package com.coolboots.qureka.predGuru.service;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.coolboots.qureka.predGuru.handler.Response;
import com.coolboots.qureka.predGuru.model.MatchEventPlainPojo;

public interface RecentWinnerService {
		
	Response recentPredictionWinnerApiCall(Long matchId);
	
	Collection<MatchEventPlainPojo> finalResults(HttpServletRequest request);
	
}
