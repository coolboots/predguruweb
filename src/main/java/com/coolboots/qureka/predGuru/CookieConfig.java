package com.coolboots.qureka.predGuru;

import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.WebUtils;

public class CookieConfig {

	public static void connect(HttpServletResponse response, String key, String value) {
    	
		
		
    	Cookie cookie = new Cookie(key, value);
    	cookie.setMaxAge(365 * 24 * 60 * 60); // expires in 367 days
    	/*cookie.setSecure(true);
    	cookie.setHttpOnly(true);
    	cookie.setPath("/");*/ 
    	response.addCookie(cookie);
	}
	
	
	public static void connect1(HttpServletResponse response, String key, String value) {
    	
    	Cookie cookie = new Cookie(key, value);
    	cookie.setMaxAge(15*60*1000); // expires in 1 minute
    	/*cookie.setSecure(true);
    	cookie.setHttpOnly(true);
    	cookie.setPath("/");*/ 
    	response.addCookie(cookie);
	}
	
	public static Optional<String> getCookie(HttpServletRequest request, String key) {
    	
		Cookie cookie 		= null;
        Cookie[] cookies	= null;
        boolean isPresent	= false;
        
        cookies = request.getCookies();
        
		for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if(cookie.getName().equals("first_name")) {
                isPresent = true;
            }
        }
		
		return Arrays.stream(request.getCookies())
			      .filter(c -> key.equals(c.getName()))
			      .map(Cookie::getValue)
			      .findAny();
	}

	public static String getCookies(HttpServletRequest request, String key) {
    	
	    Cookie cookieKeyValue = WebUtils.getCookie(request, key);
	    
	    if(cookieKeyValue != null) {
	    	return cookieKeyValue.getValue();
	    }
	    return null;
    	
	}
	
	public static void remove(HttpServletRequest request, HttpServletResponse response) {
		
		Cookie getCookie 	= null;
        Cookie[] cookies	= null;
        boolean isPresent	= false;
        
        cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++) {
			getCookie = cookies[i];
            Cookie cookie = new Cookie(getCookie.getName(), null);
            cookie.setMaxAge(0);
    		/*cookie.setSecure(true);
    		cookie.setHttpOnly(true);
    		cookie.setPath("/");*/
    		response.addCookie(cookie);
        }
	}
	
	
}
