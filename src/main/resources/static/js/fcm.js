
	var config = {
        'messagingSenderId': '288547709216',
        'apiKey': 'AIzaSyANLKjJv-60pF9DaVuilDE4aNRyrjP_InA',
        'projectId': 'predguru-2e747',
        'appId': '1:288547709216:web:3edd8273c34a60ace83745',
    };

    firebase.initializeApp(config);
	const messaging = firebase.messaging();
	
	messaging.onTokenRefresh(() => {
	    messaging.getToken().then((refreshedToken) => {
	      	setTokenSentToServer(false);
	      	sendTokenToServer(refreshedToken);
	    }).catch((err) => {
	      	console.log('Unable to retrieve refreshed token ', err);
	    });
	});
	
	function sendTokenToServer(currentToken) {
			$.ajax({
		        url: '/user-rest/update-fcm-token?token='+currentToken,
		        method: 'GET',
		        contentType : 'application/json; charset=utf-8',
				success: function(results) {					
					if(results=='sucess'){
						setTokenSentToServer(true);
						document.cookie = "fcm_token="+currentToken+";";
						//document.cookie = "fcm_token= yes;" + expires + ";path=/";
						//$.cookie("fcm_token", currentToken);
					}
		        }    
		    });
	    
	}
		
	function isTokenSentToServer() {
	    return window.localStorage.getItem('sentToServer') === '1';
	}
	
	function setTokenSentToServer(sent) {
		window.localStorage.setItem('sentToServer', sent ? '1' : '0');
	}
	
	requestPermission();
	
	function requestPermission() {
		
	    Notification.requestPermission().then((permission) => {
	      	if (permission === 'granted') {
		        resetUI();
	      	} else {
				setTokenSentToServer(false);
	        	console.log('Unable to get permission to notify.');
	      	}
	    });

	}

	function resetUI() {
		messaging.getToken().then((currentToken) => {
			
		//console.log(" cookiee : " +$.cookie("fcm_token"));
	 	//console.log(currentToken);
		//$("#token").text(currentToken);
		var token_in_cookie  = getCookie("fcm_token");
		//console.log(currentToken);
		//console.log(token_in_cookie);
		  if (currentToken) {			
			if ((!isTokenSentToServer()) || (token_in_cookie!==currentToken)) {
				//$("#token").text(currentToken);
		    	sendTokenToServer(currentToken);
			} else {
				console.log(currentToken);
		      	console.log('Token already sent to server so won\'t send it again ' +
		          'unless it changes');
		    }
		  } else {
		    console.log('No Instance ID token available. Request permission to generate one.');
		    setTokenSentToServer(false);
		  }
		}).catch((err) => {
			console.log(err);
		  console.log('An error occurred while retrieving token. ', err);
		  setTokenSentToServer(false);
		});	
	}
	
	let enableForegroundNotification = true;
   	messaging.onMessage(function(payload) {
       	console.log("Message received. ", payload);
       	if(enableForegroundNotification) {
           	const {title, ...options} = JSON.parse(payload.data.notification);
				
			navigator.serviceWorker.getRegistrations().then(registration => {
	           	console.log(options);
				registration[0].showNotification(title, options);
           	});
			//return self.registration.showNotification(title,options);
       	}
   	});


