function statusChangeCallback(response) { // Called with the results from FB.getLoginStatus().
	//alert("Statuschange callback called");
	console.log('Status Change Callback');
	console.log(response); // The current login status of the person.
	if (response.status === 'connected') { // Logged into your webpage and Facebook.
		testAPI();
	} else { // Not logged into your webpage or we are unable to tell.
		//document.getElementById('status').innerHTML = 'Please log ' + 'into this webpage.';
		if (response.status === 'not_authorized' || !response || response.error ) {
			//alert("Oops! Something Went Wrong")
		  }
	}
}


function checkLoginState() { // Called when a person is finished with the Login Button.	
	console.log("checkLoginState");
	FB.getLoginStatus(function (response) { // See the onlogin handler
		statusChangeCallback(response);
	});

}


window.fbAsyncInit = function () {
	//alert("fbAsyncInit");
	FB.init({
		//appId: '744736366425884',4055083231199281, 1215950315465818
		appId: '835884813807700',
		//appId:'4055083231199281',
		cookie: true, // Enable cookies to allow the server to access the session.
		xfbml: true, // Parse social plugins on this webpage.
		version: 'v9.0' // Use this Graph API version for this call.
	});


	FB.getLoginStatus(function (response) { // Called after the JS SDK has been initialized.
		console.log("loginstatus");
		console.log(response);
		statusChangeCallback(response); // Returns the login status.
	});
};

function testAPI() {
	//alert("Test");
	var subdomain = "";
	if (window.location.hostname == "103.94.108.147") {
		var subdomain = "/pred";
	} // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
	//console.log('Welcome!  Fetching your information.... ');
	FB.api('/me?fields=id,name,email', function (response) {
		console.log(response);
		//alert("fb hello");
		console.log('Successful login for: ' + response.name);
		console.log('json response : ' + JSON.stringify(response));
		var email=response.email;
		var hostname = window.location.hostname;
		if ( typeof(email) == "undefined" || email == null || email==="") {
			email=response.id+"@PG.com";
		}
		console.log('current user email: ' + response.email);
		var newData = {
			"firstName": response.name,
			"email": email,
			"info3": hostname,
			"info1": "facebook"
		};
		var dataJson = JSON.stringify(newData);
		$.ajax({
			type: "POST",
			contentType: "application/json;",
			url: subdomain + "/signup",
			data: dataJson,
			cache: false,
			success: function (data) {
				window.location.href = subdomain + '/';
				console.log("SUCCESS : ", data);
			},
			error: function (e) {
				console.log("ERROR : ", e);
			}
		});

		//document.getElementById('status').innerHTML ='Thanks for logging in, ' + response.name + '!';
	});
}




$("#test").click('click', function () {
	//alert("Hellofb");
	//do the login
	FB.login(statusChangeCallback, {
		scope: 'email,public_profile',
		return_scopes: true
	});
}, false);