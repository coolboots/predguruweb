/*
function init() {
  gapi.load('auth2', function() {
	
  });
}
*/
function onSuccess(googleUser) {
	var profile = googleUser.getBasicProfile();
	console.log(profile);
	console.log('Logged in as: ' + profile.getName());
	var name = profile.getName();
	var email = profile.getEmail();
	var loginForm = "google";
	var hostname = window.location.hostname;

	var newData = {
		"firstName" : name,
		"email" : email,
		"info3" : hostname,
		"info1" : loginForm
	};

	var dataJson = JSON.stringify(newData);

	console.log(dataJson);
	$("body").addClass("loading");

	$.ajax({
		type: "POST",
		contentType: "application/json;",
		url: subdomain + "/signup",
		data: dataJson,
		cache: false,
		success: function (response) {
			console.log(response);
			//alert(response.status);
			if (response!=null && response.status == 200) {
				var id_token = googleUser.getAuthResponse().access_token;

				console.log("ID Token: " + id_token);
       			//localStorage.setItem("google_access_token", id_token);
				//alert("success");
				window.location.href = subdomain + '/';
				googleUser.disconnect();
				$("body").removeClass("loading");
			} else if (response!=null && response.status == 999) {
				//alert("999");
				 $("#myModal").modal('show');
				 $("#error-msg").html(response.message);
				googleUser.disconnect();
				window.location.href = subdomain + '/signup';
				$("body").removeClass("loading");
			} else {
                //alert("success else");
                window.location.href = subdomain + '/signup';
                $("body").removeClass("loading");
            }
		},
		error : function(e) {
			$("body").removeClass("loading");
		}
	});
	
}
function onFailure(error) {
	console.log(error);
}
function renderButton() {
	gapi.signin2.render('gmail-login', {
		'scope' : 'profile email',
		'longtitle' : true,
		'theme' : 'dark',
		'onsuccess' : onSuccess,
		'onfailure' : onFailure
	});
}